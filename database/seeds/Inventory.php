<?php

use Illuminate\Database\Seeder;

class Inventory extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
    	$faker = Faker\Factory::create();
    	foreach (range(1,5000) as $index) {

				$autoid = $faker->numberBetween( 1, 5000);
				$user = DB::table('edmunds_make')->where('id', $autoid )->first();

		        DB::table('inventory')->insert([
		            'autoid'    => $autoid,
		            'description' => $user->year. ' '.$user->make.' '.$user->model ,
		            'purchaseprice'  => $faker->randomFloat( 2, 5000 , 15000 ),
		            'soldprice'  => $faker->randomFloat( 2, 17000 , 51000 ),
		            'mileage'   => $faker->numberBetween(1000, 59000) ,
		            'purchasedate'      => date("Y-m-d H:i:s" , strtotime( "-" .rand(45, 150) . " days" ) ) ,
		            'solddate'      => date("Y-m-d H:i:s" , strtotime( "-" .rand(1, 30) . " days" ) ) ,
		            'createdBy'    => $faker->randomElement(array ('88','121','129','130'))  ,
		            'created_at'    => date("Y-m-d H:i:s")  ,
		            'starred'    => $faker->randomElement(array ('0','1'))  ,
		        ]);
        }          
        
    	foreach (range(1,5000) as $index) {

				$autoid = $faker->numberBetween( 1, 5000);
				$user = DB::table('edmunds_make')->where('id', $autoid )->first();

		        DB::table('inventory')->insert([
		            'autoid'    => $autoid,
		            'description' => $user->year. ' '.$user->make.' '.$user->model ,
		            'purchaseprice'  => $faker->randomFloat( 2, 5000 , 51000 ),
		            'soldprice'  => $faker->randomFloat( 2, 5000 , 51000 ),
		            'tradeinid'  => $faker->numberBetween( 1, 15000),
		            'tradeinprice'  => $faker->randomFloat( 2, 100 , 4000 ),
		            'mileage'   => $faker->numberBetween(1000, 59000) ,
		            'purchasedate'      => date("Y-m-d H:i:s" , strtotime( "-" .rand(45, 150) . " days" ) ) ,
		            'solddate'      => date("Y-m-d H:i:s" , strtotime( "-" .rand(1, 30) . " days" ) ) ,
		            'tradeindate'      => date("Y-m-d H:i:s" , strtotime( "-" .rand(45, 150) . " days" ) ) ,
		            'createdBy'    => $faker->randomElement(array ('88','121','129','130'))  ,
		            'created_at'    => date("Y-m-d H:i:s")  ,
		            'starred'    => $faker->randomElement(array ('0','1'))  ,
		        ]);
        }   
        
       
        
    }
}
