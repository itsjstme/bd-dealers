<?php


use Illuminate\Database\Seeder;



class Dealership extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        
    	$faker = Faker\Factory::create();
    	foreach (range(1,10) as $index) {
	        $email = $faker->email;
	        $f =  new Faker\Provider\en_US\PhoneNumber($faker);
	        
	        
	        DB::table('users')->insert([
	            'first_name' => $faker->firstName,
	            'last_name'  => $faker->lastName,
	            'username'   => $email ,
	            'email'      => $email ,
	            'address'    => $faker->address,
	            'country'    => $faker->country,
	            'phone'      => $faker->phoneNumber ,
	            'role'       => 'Dealership',
	            'password'   => bcrypt('secret'),
	        ]);
        }        
        
    }
}
