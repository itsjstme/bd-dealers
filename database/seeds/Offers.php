<?php

use Illuminate\Database\Seeder;

class Offers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        
    	$faker = Faker\Factory::create();
    	foreach (range(1,5000) as $index) {
            
            $createdBy = $faker->randomElement(array ('88','121','129','130'));    
            $temp = array();
            for($i = 0  ; $i < 6 ; $i++ ){
                $temp[] = $faker->numberBetween(1, 7000);
            }
	        
	        DB::table('offers')->insert([
	            'offerids' => implode("," , $temp ) ,
	            'title'  => $faker->sentence(6),
	            'price'   => $faker->randomFloat( 2, 17000 , 51000 ),
	            'expires'      => date("Y-m-d H:i:s" , strtotime( "+". $faker->numberBetween( 15, 33) ." days"))  ,
	            'createdBy'    =>  $createdBy ,
	        ]);
        }         
        
        
    }
}
