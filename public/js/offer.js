/**
 * User: laurentrenard
 * Date: 10/3/13
 * Time: 9:55 PM
 **/
var app = angular.module('app', ['ui.bootstrap','smart-table']);


app.controller('pipeCtrl', ['Resource', '$scope' , function (service , $scope ) {

  var ctrl = this;

  this.displayed = [];

  $scope.myFunc = function() {
      alert('qwewqe');
  };

  this.callServer = function callServer(tableState) {

    ctrl.isLoading = true;

    var pagination = tableState.pagination;

    var start = pagination.start || 0;
    var number = pagination.number || 10;

    service.getPage(start, number, tableState).then(function (result) {
      ctrl.displayed = result.data;
      tableState.pagination.numberOfPages = result.numberOfPages;//set the number of pages so the pagination can update
      ctrl.isLoading = false;
    });
  };

}])
  .factory('Resource', ['$q', '$http' , '$filter', '$timeout', function ($q, $http , $filter, $timeout) {

    //this would be the service to call your server, a standard bridge between your model an $http

    // the database (normally on your server)
    var randomsItems = [];

    function createRandomItem(object) {
      return {
        id: object.id,
        name: object.description,
        age: object.mileage ,
        saved: object.purchaseprice
      };

    }
    
    $http.get("/getinventory")
    .then(function(response) {
        //First function handles success
        //$scope.content = response.data;
        //randomsItems = response.data
        angular.forEach(response.data.data, function(value, key) {
            randomsItems.push(createRandomItem(value));
        });        
        
    }, function(response) {
        //Second function handles error
        //$scope.content = "Something went wrong";

    });
    

    //fake call to the server, normally this service would serialize table state to send it to the server (with query parameters for example) and parse the response
    //in our case, it actually performs the logic which would happened in the server
    function getPage(start, number, params) {

      var deferred = $q.defer();

      var filtered = params.search.predicateObject ? $filter('filter')(randomsItems, params.search.predicateObject) : randomsItems;

      if (params.sort.predicate) {
        filtered = $filter('orderBy')(filtered, params.sort.predicate, params.sort.reverse);
      }

      var result = filtered.slice(start, start + number);

      $timeout(function () {
        //note, the server passes the information about the data set size
        deferred.resolve({
          data: result,
          numberOfPages: Math.ceil(filtered.length / number)
        });
      }, 1500);


      return deferred.promise;
    }

    return {
      getPage: getPage
    };


  }]);



