(function(angular) {
  'use strict';

  angular.module('app', [])
    .controller('MainCtrl', [ '$window' ,'$scope', '$http', function ( $window , $scope, $http) {
      
      $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
      
      var year = new Date().getFullYear();
      var range = [];

      for (var i = 1990; i < year + 1; i++) {
        range.push({
          label: i ,
          value: parseInt(i)
        });
      }

      $scope.years = range;
      $scope.year = '';

		$scope.submitForm = function() {

			// check to make sure the form is completely valid
			if ($scope.userForm.$valid) {
				alert('our form is amazing');
			}

		};

    $scope.patternPhoneNumber = (function() {
      var regexp = /^[A-HJ-NPR-Z\\d]{8}[\\dX][A-HJ-NPR-Z\\d]{2}\\d{6}$/;
      return {
        test: function(value) {
          if ($scope.inventory.vin === false) {
            alert('QWEWQE');
            return true;
          }
          alert('hi');
          return regexp.test(value);
        }
      };
    })();


    /*   FUNCTIONS   */

    
    $scope.vinNumberSearch = function(){
   
        $http.get('/inventory/findByVin/' + $scope.inventory.vin) 
          .then(function(result) {
             $scope.carName =  result.data[0].searchOn;
             $scope.inventory.styleid =  result.data[0].styleID;
        });
    }

    $scope.changeYear=function(item){
        $scope.theYear =  item;
        $scope.makes = null;
        $http.get('/year/' + $scope.year) 
          .then(function(result) {
              var range = [];
            for(var i=0; i < result.data.length; i++){
                  range.push({
                    label: result.data[i].make ,
                    value: result.data[i].make
                  });                
                
            }
            
            $scope.makes = range;

        });        
    }     
    
    $scope.changeMake=function(item){
      $scope.theMake = item;
      $scope.models = null;
        $http.get('/year/'+$scope.theYear+'/make/'+item) 
          .then(function(result) {
              var range = [];
            for(var i=0; i < result.data.length; i++){
                  range.push({
                    label: result.data[i].model ,
                    value: result.data[i].styleid
                  });                
                
            }
            
            $scope.models = range;

        });          
    }   
    
    $scope.changeModel=function(item){
      $scope.trims = null;
        $http.get('/year/'+$scope.year+'/make/'+$scope.theMake+'/model/'+item) 
          .then(function(result) {
              var range = [];
            for(var i=0; i < result.data.length; i++){
                  range.push({
                    label: result.data[i].model ,
                    value: result.data[i].styleid
                  });                
                
            }
            
            $scope.trims = range;

        });         
    }   
    
    $scope.searchInventory=function(item){
      $scope.trims = null;
        $http.get('/year/'+$scope.year+'/make/'+$scope.theMake+'/model/'+item) 
          .then(function(result) {
              var range = [];
            for(var i=0; i < result.data.length; i++){
                  range.push({
                    label: result.data[i].model ,
                    value: result.data[i].styleid
                  });                
                
            }
            
            $scope.trims = range;

        });         
    }    
    
      
    
    $scope.addInventory = function(inventory){
      

        $http({
            url: '/inventory/save',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
            method: "POST",
            data: inventory
        })
        .then(function(response) {
                // success
            if(response.data.styleid){
              window.location.href = '/inventory';
            }
        }, 
        function(response) { // optional
                // failed
        });
      
    }  
      
      
    }]);
})(window.angular);