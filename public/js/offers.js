var app = angular.module('app', ['ngTouch', 'ui.grid', 'ui.grid.pagination']);

app.directive("matchPassword", function () {
    return {
        require: "ngModel",
        scope: {
            otherModelValue: "=matchPassword"
        },
        link: function(scope, element, attributes, ngModel) {

            ngModel.$validators.matchPassword = function(modelValue) {
                return modelValue == scope.otherModelValue;
            };

            scope.$watch("otherModelValue", function() {
                ngModel.$validate();
            });
        }
    };
});

app.controller('MainCtrl', ['$scope','uiGridConstants', '$http', function ($scope, uiGridConstants, $http) {
var data = [];
 
$scope.gridOptions = {
    showGridFooter: true,
    showColumnFooter: true,
    enableFiltering: true,
    enableRowSelection: true,
    columnDefs: [
        { name: 'status', displayName: 'Active', type: 'boolean',cellTemplate: '<input type="checkbox" ng-model="row.entity.status">'},
        { field: 'year', width: '13%' , displayName: 'Year' },
        { field: 'make',aggregationType: uiGridConstants.aggregationTypes.sum, width: '13%' , displayName: 'Make' },
        { field: 'model', width: '13%' ,  displayName: 'Model' },
        { field: 'trim',  width: '13%' ,  displayName: 'Trim' },
        { field: 'mileage',  width: '13%' ,  displayName: 'Mileage' },
        { name: 'ageMin', field: 'purchaseprice' , width: '13%', displayName: 'Price' },
        { name: 'ageMax', field: 'purchasedate', cellFilter: 'date' , width: '13%', displayName: 'Purchased' }
    ],
    data: data,
    onRegisterApi: function(gridApi) {
            $scope.gridApi = gridApi;
    }
};

$scope.gridOptions.multiSelect = true;
 
$scope.toggleFooter = function() {
  $scope.gridOptions.showGridFooter = !$scope.gridOptions.showGridFooter;
  $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
};
 
$scope.toggleColumnFooter = function() {
  $scope.gridOptions.showColumnFooter = !$scope.gridOptions.showColumnFooter;
  $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
};
 
$http.get('/getinventory')
  .success(function(data) {
    data.forEach( function(row) {
      row.registered = Date.parse(row.registered);
    });
    $scope.gridOptions.data = data;
  });
}]);