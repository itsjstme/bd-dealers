<?php

namespace App;


use Auth;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class offers
 * @package App\Models
 * @version March 19, 2017, 8:19 pm UTC
 */
class ViewInventory extends Model
{
    

    public $table = 'view_inventory';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * Validation rules
     *
     * @var array
     */
  
}
