<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOffersRequest;
use App\Http\Requests\UpdateOffersRequest;
use App\Repositories\OffersRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests;
use App\User;
use App\Message;
use DB;
use Auth;
use Session;
use App\Foo;
use View;


class OffersController extends AppBaseController
{


    /** @var  DealershipUsersRepository */
    private $offersRepository;
    
	protected $foo;
	
	public function __construct(Foo $foo , OffersRepository $offersRepository){		
       $this->middleware('auth');	 
	   $this->foo = $foo;
	   $this->offersRepository = $offersRepository;
	   //$this->middleware('permission:users.manage');
    }

    /**
     * Display a listing of the Offers.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->offersRepository->pushCriteria(new RequestCriteria($request));
        $offers = $this->offersRepository->all();

        return view('offers.index')
            ->with('offers', $offers);
    }

    /**
     * Show the form for creating a new Offers.
     *
     * @return Response
     */
    public function create()
    {
        

        //var_dump(Auth::user()->getDealershipId());
        //die();
        
        return view('offers.create');
    }

    /**
     * Store a newly created Offers in storage.
     *
     * @param CreateOffersRequest $request
     *
     * @return Response
     */
    public function store(CreateOffersRequest $request)
    {
        $input = $request->all();

        $offers = $this->offersRepository->create($input);

        Flash::success('Offers saved successfully.');

        return redirect(route('offers.index'));
    }

    /**
     * Display the specified Offers.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $offers = $this->offersRepository->findWithoutFail($id);

        if (empty($offers)) {
            Flash::error('Offers not found');

            return redirect(route('offers.index'));
        }

        return view('offers.show')->with('offers', $offers);
    }

    /**
     * Show the form for editing the specified Offers.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $offers = $this->offersRepository->findWithoutFail($id);

        if (empty($offers)) {
            Flash::error('Offers not found');

            return redirect(route('offers.index'));
        }

        return view('offers.edit')->with('offers', $offers);
    }

    /**
     * Update the specified Offers in storage.
     *
     * @param  int              $id
     * @param UpdateOffersRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOffersRequest $request)
    {
        $offers = $this->offersRepository->findWithoutFail($id);

        if (empty($offers)) {
            Flash::error('Offers not found');

            return redirect(route('offers.index'));
        }

        $offers = $this->offersRepository->update($request->all(), $id);

        Flash::success('Offers updated successfully.');

        return redirect(route('offers.index'));
    }

    /**
     * Remove the specified Offers from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $offers = $this->offersRepository->findWithoutFail($id);

        if (empty($offers)) {
            Flash::error('Offers not found');

            return redirect(route('offers.index'));
        }

        $this->offersRepository->delete($id);

        Flash::success('Offers deleted successfully.');

        return redirect(route('offers.index'));
    }
}
