<?php

namespace App\Http\Controllers\Auth;
use App\User;
use App\Models\Dealership;
use App\Models\DealershipUsers;
use Validator;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Cartalyst\Stripe\Stripe;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */
    
    private $stripe;

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        $this->stripe = new Stripe('sk_test_zKw6xsFn2KaDxNQGj1E30Yy9', '2017-02-14');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return  Validator::make($data, [
            'first_name' => 'required',
            'last_name' => 'required',
            'dealershipname' => 'required',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
		
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $customer = null;
        
        try {
            
            DB::beginTransaction();
            //do something
             $savedata = User::create([
    		    'status'     => 'Active',
                'first_name' => $data['first_name'],
                'last_name'  => $data['last_name'],
                'role'       => 'Dealership',
                'username'   => $data['email'],
                'email'      => $data['email'],
                'password'   => bcrypt($data['password'])
            ]);
            
           $insertedId = $savedata->id;
            
           $dealership = Dealership::create([
                'name' => $data['dealershipname'],
                'createdBy' => $savedata->id ,
            ]);  
        
            $customer = $this->stripe->customers()->create([
                'email' =>  $data['email'] ,
                'metadata' => [ 'dealership' => $data['dealershipname'] , 'firstName' => $data['first_name'] , 'lastName' => $data['last_name'] ],
                'plan'  => 'basic'
            ]);	
            
            
    
            $savedata->stripecutomerId = $customer['id'] ;
            $savedata->	dealershipId = $dealership->id ;
            $savedata->save();        

            $create = App/DealershipUsers::create();
            
           $dealership = DealershipUsers::create([
                'userid' => $insertedId ,
                'dealershipid' => $dealership->id ,
            ]);              

    		DB::table('role_user')->insert(['user_id' => $insertedId, 'role_id' => 3]);
    		DB::commit();
    		
                          
            
        } catch (\Exception $e) {
            DB::rollback();
            
            if(isset($customer['id']))
                $this->stripe->customers()->delete($customer['id']);
                
            
            
        }        
         
      
         return $savedata;
  

	}
	
	
}
