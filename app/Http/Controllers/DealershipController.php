<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDealershipRequest;
use App\Http\Requests\UpdateDealershipRequest;
use App\Repositories\DealershipRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class DealershipController extends AppBaseController
{
    /** @var  DealershipRepository */
    private $dealershipRepository;

    public function __construct(DealershipRepository $dealershipRepo)
    {
        $this->dealershipRepository = $dealershipRepo;
    }

    /**
     * Display a listing of the Dealership.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->dealershipRepository->pushCriteria(new RequestCriteria($request));
        $dealerships = $this->dealershipRepository->all();

        return view('dealerships.index')
            ->with('dealerships', $dealerships);
    }

    /**
     * Show the form for creating a new Dealership.
     *
     * @return Response
     */
    public function create()
    {
        return view('dealerships.create');
    }

    /**
     * Store a newly created Dealership in storage.
     *
     * @param CreateDealershipRequest $request
     *
     * @return Response
     */
    public function store(CreateDealershipRequest $request)
    {
        $input = $request->all();

        $dealership = $this->dealershipRepository->create($input);

        Flash::success('Dealership saved successfully.');

        return redirect(route('dealerships.index'));
    }

    /**
     * Display the specified Dealership.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $dealership = $this->dealershipRepository->findWithoutFail($id);

        if (empty($dealership)) {
            Flash::error('Dealership not found');

            return redirect(route('dealerships.index'));
        }

        return view('dealerships.show')->with('dealership', $dealership);
    }

    /**
     * Show the form for editing the specified Dealership.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $dealership = $this->dealershipRepository->findWithoutFail($id);

        if (empty($dealership)) {
            Flash::error('Dealership not found');

            return redirect(route('dealerships.index'));
        }

        return view('dealerships.edit')->with('dealership', $dealership);
    }

    /**
     * Update the specified Dealership in storage.
     *
     * @param  int              $id
     * @param UpdateDealershipRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDealershipRequest $request)
    {
        $dealership = $this->dealershipRepository->findWithoutFail($id);

        if (empty($dealership)) {
            Flash::error('Dealership not found');

            return redirect(route('dealerships.index'));
        }

        $dealership = $this->dealershipRepository->update($request->all(), $id);

        Flash::success('Dealership updated successfully.');

        return redirect(route('dealerships.index'));
    }

    /**
     * Remove the specified Dealership from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $dealership = $this->dealershipRepository->findWithoutFail($id);

        if (empty($dealership)) {
            Flash::error('Dealership not found');

            return redirect(route('dealerships.index'));
        }

        $this->dealershipRepository->delete($id);

        Flash::success('Dealership deleted successfully.');

        return redirect(route('dealerships.index'));
    }
}
