<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateOffersAPIRequest;
use App\Http\Requests\API\UpdateOffersAPIRequest;
use App\Models\Offers;
use App\Repositories\OffersRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class OffersController
 * @package App\Http\Controllers\API
 */

class OffersAPIController extends AppBaseController
{
    /** @var  OffersRepository */
    private $offersRepository;

    public function __construct(OffersRepository $offersRepo)
    {
        $this->offersRepository = $offersRepo;
    }

    /**
     * Display a listing of the Offers.
     * GET|HEAD /offers
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->offersRepository->pushCriteria(new RequestCriteria($request));
        $this->offersRepository->pushCriteria(new LimitOffsetCriteria($request));
        $offers = $this->offersRepository->all();

        return $this->sendResponse($offers->toArray(), 'Offers retrieved successfully');
    }

    /**
     * Store a newly created Offers in storage.
     * POST /offers
     *
     * @param CreateOffersAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateOffersAPIRequest $request)
    {
        $input = $request->all();

        $offers = $this->offersRepository->create($input);

        return $this->sendResponse($offers->toArray(), 'Offers saved successfully');
    }

    /**
     * Display the specified Offers.
     * GET|HEAD /offers/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Offers $offers */
        $offers = $this->offersRepository->findWithoutFail($id);

        if (empty($offers)) {
            return $this->sendError('Offers not found');
        }

        return $this->sendResponse($offers->toArray(), 'Offers retrieved successfully');
    }

    /**
     * Update the specified Offers in storage.
     * PUT/PATCH /offers/{id}
     *
     * @param  int $id
     * @param UpdateOffersAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOffersAPIRequest $request)
    {
        $input = $request->all();

        /** @var Offers $offers */
        $offers = $this->offersRepository->findWithoutFail($id);

        if (empty($offers)) {
            return $this->sendError('Offers not found');
        }

        $offers = $this->offersRepository->update($input, $id);

        return $this->sendResponse($offers->toArray(), 'Offers updated successfully');
    }

    /**
     * Remove the specified Offers from storage.
     * DELETE /offers/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Offers $offers */
        $offers = $this->offersRepository->findWithoutFail($id);

        if (empty($offers)) {
            return $this->sendError('Offers not found');
        }

        $offers->delete();

        return $this->sendResponse($id, 'Offers deleted successfully');
    }
}
