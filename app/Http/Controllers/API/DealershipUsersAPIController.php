<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDealershipUsersAPIRequest;
use App\Http\Requests\API\UpdateDealershipUsersAPIRequest;
use App\Models\DealershipUsers;
use App\Repositories\DealershipUsersRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class DealershipUsersController
 * @package App\Http\Controllers\API
 */

class DealershipUsersAPIController extends AppBaseController
{
    /** @var  DealershipUsersRepository */
    private $dealershipUsersRepository;

    public function __construct(DealershipUsersRepository $dealershipUsersRepo)
    {
        $this->dealershipUsersRepository = $dealershipUsersRepo;
    }

    /**
     * Display a listing of the DealershipUsers.
     * GET|HEAD /dealershipUsers
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->dealershipUsersRepository->pushCriteria(new RequestCriteria($request));
        $this->dealershipUsersRepository->pushCriteria(new LimitOffsetCriteria($request));
        $dealershipUsers = $this->dealershipUsersRepository->all();

        return $this->sendResponse($dealershipUsers->toArray(), 'Dealership Users retrieved successfully');
    }

    /**
     * Store a newly created DealershipUsers in storage.
     * POST /dealershipUsers
     *
     * @param CreateDealershipUsersAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateDealershipUsersAPIRequest $request)
    {
        $input = $request->all();

        $dealershipUsers = $this->dealershipUsersRepository->create($input);

        return $this->sendResponse($dealershipUsers->toArray(), 'Dealership Users saved successfully');
    }

    /**
     * Display the specified DealershipUsers.
     * GET|HEAD /dealershipUsers/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var DealershipUsers $dealershipUsers */
        $dealershipUsers = $this->dealershipUsersRepository->findWithoutFail($id);

        if (empty($dealershipUsers)) {
            return $this->sendError('Dealership Users not found');
        }

        return $this->sendResponse($dealershipUsers->toArray(), 'Dealership Users retrieved successfully');
    }

    /**
     * Update the specified DealershipUsers in storage.
     * PUT/PATCH /dealershipUsers/{id}
     *
     * @param  int $id
     * @param UpdateDealershipUsersAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDealershipUsersAPIRequest $request)
    {
        $input = $request->all();

        /** @var DealershipUsers $dealershipUsers */
        $dealershipUsers = $this->dealershipUsersRepository->findWithoutFail($id);

        if (empty($dealershipUsers)) {
            return $this->sendError('Dealership Users not found');
        }

        $dealershipUsers = $this->dealershipUsersRepository->update($input, $id);

        return $this->sendResponse($dealershipUsers->toArray(), 'DealershipUsers updated successfully');
    }

    /**
     * Remove the specified DealershipUsers from storage.
     * DELETE /dealershipUsers/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var DealershipUsers $dealershipUsers */
        $dealershipUsers = $this->dealershipUsersRepository->findWithoutFail($id);

        if (empty($dealershipUsers)) {
            return $this->sendError('Dealership Users not found');
        }

        $dealershipUsers->delete();

        return $this->sendResponse($id, 'Dealership Users deleted successfully');
    }
}
