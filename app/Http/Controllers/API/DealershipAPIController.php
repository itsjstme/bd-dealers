<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDealershipAPIRequest;
use App\Http\Requests\API\UpdateDealershipAPIRequest;
use App\Models\Dealership;
use App\Repositories\DealershipRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class DealershipController
 * @package App\Http\Controllers\API
 */

class DealershipAPIController extends AppBaseController
{
    /** @var  DealershipRepository */
    private $dealershipRepository;

    public function __construct(DealershipRepository $dealershipRepo)
    {
        $this->dealershipRepository = $dealershipRepo;
    }

    /**
     * Display a listing of the Dealership.
     * GET|HEAD /dealerships
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->dealershipRepository->pushCriteria(new RequestCriteria($request));
        $this->dealershipRepository->pushCriteria(new LimitOffsetCriteria($request));
        $dealerships = $this->dealershipRepository->all();

        return $this->sendResponse($dealerships->toArray(), 'Dealerships retrieved successfully');
    }

    /**
     * Store a newly created Dealership in storage.
     * POST /dealerships
     *
     * @param CreateDealershipAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateDealershipAPIRequest $request)
    {
        $input = $request->all();

        $dealerships = $this->dealershipRepository->create($input);

        return $this->sendResponse($dealerships->toArray(), 'Dealership saved successfully');
    }

    /**
     * Display the specified Dealership.
     * GET|HEAD /dealerships/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Dealership $dealership */
        $dealership = $this->dealershipRepository->findWithoutFail($id);

        if (empty($dealership)) {
            return $this->sendError('Dealership not found');
        }

        return $this->sendResponse($dealership->toArray(), 'Dealership retrieved successfully');
    }

    /**
     * Update the specified Dealership in storage.
     * PUT/PATCH /dealerships/{id}
     *
     * @param  int $id
     * @param UpdateDealershipAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDealershipAPIRequest $request)
    {
        $input = $request->all();

        /** @var Dealership $dealership */
        $dealership = $this->dealershipRepository->findWithoutFail($id);

        if (empty($dealership)) {
            return $this->sendError('Dealership not found');
        }

        $dealership = $this->dealershipRepository->update($input, $id);

        return $this->sendResponse($dealership->toArray(), 'Dealership updated successfully');
    }

    /**
     * Remove the specified Dealership from storage.
     * DELETE /dealerships/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Dealership $dealership */
        $dealership = $this->dealershipRepository->findWithoutFail($id);

        if (empty($dealership)) {
            return $this->sendError('Dealership not found');
        }

        $dealership->delete();

        return $this->sendResponse($id, 'Dealership deleted successfully');
    }
}
