<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateInventoryRequest;
use App\Http\Requests\UpdateInventoryRequest;
use App\Repositories\InventoryRepository;
use App\Models\Inventory;
use App\Models\EdmundMakeModel;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use DB;
use App\User;
use Auth;
use Session;
use App\Foo;


class InventoryController extends AppBaseController
{
	  /**
     * Display a listing of the __construct.
     *
     * @return \Illuminate\Http\Response
     */
     private $inventoryRepository;
	 private $activities;
	 protected $foo;
	 
    public function __construct(Foo $foo , InventoryRepository $inventoryRepo)
    {
        $this->middleware('auth');	 
	    $this->foo = $foo;        
        $this->inventoryRepository = $inventoryRepo;
        DB::enableQueryLog();
    }


    /**
     * Display a listing of the Inventory.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        
	   $searchname = $request->input('search');
	   $inventories = DB::table('inventory')
	           ->where('dealershipid', '=', Auth::user()->getDealershipId() )
               ->where(function ($query) use ($searchname) {
                   
                   if(!empty($searchname))
                    $query->whereRaw("description like '%{$searchname}%'");
               })->orderBy('id', 'DESC')->paginate(20);  	   
	   
	  
        return view('inventories.index',compact('inventories')); 
        
    }
    /*
     * Display a listing of the Inventory.
     *
     * @param Request $request
     * @return Response
     */
    public function paginate(Request $request)
    {
        
	   $searchname = $request->input('search');
	   $inventories = DB::table('inventory')
	           ->where('dealershipid', '=', Auth::user()->getDealershipId() )
               ->where(function ($query) use ($searchname) {
                   
                   if(!empty($searchname))
                    $query->whereRaw("description like '%{$searchname}%'");
               })->orderBy('id', 'DESC')->paginate(20);  	   
	   
	  
	    return response()->json($inventories);
        // return view('inventories.index',compact('inventories')); 
        
    }    

    /*
     * Display a listing of the Inventory.
     *
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        
	   $searchname = $request->input('search');
	   $paramters = explode(" " , $searchname);
	   
	   $inventories = DB::table('edmunds_make')
               ->where(function ($query) use ($paramters) {
                   if(isset($paramters[0]))
                        $query->where('year', '=', $paramters[0]  );
                   if(isset($paramters[1]))
                        $query->where('make', 'LIKE', "%".$paramters[1]."%"  );
                   if(isset($paramters[2])){
                        $query->where('model', 'LIKE', "%".$paramters[2]."%"  );
                   }

                        
               })->orderBy('id', 'DESC')->paginate(8);  	   
	   
	   $inventories->appends('dfgdg')->links();
	   
	  //dd(DB::getQueryLog());
	  //die();
	    return response()->json($inventories);
        // return view('inventories.index',compact('inventories')); 
        
    }

    /**
     * Show the form for creating a new Inventory.
     *
     * @return Response
     */
    public function create()
    {
        return view('inventories.create');
    }

    /**
     * Store a newly created Inventory in storage.
     *
     * @param CreateInventoryRequest $request
     *
     * @return Response
     */
    public function store(CreateInventoryRequest $request)
    {
        $input = $request->all();

        $inventory = $this->inventoryRepository->create($input);

        Flash::success('Inventory saved successfully.');

        return redirect(route('inventories.index'));
    }
    
    /**
     * Store a newly created Inventory in storage.
     *
     * @param CreateInventoryRequest $request
     *
     * @return Response
     */
    public function save(Request $request)
    {
        
         if (count($request->json()->all())) {
             $input = $request->json()->all();
         }
        
        $model = new Inventory();
        
        // attempt validation
        if ($model->validate($input))
        {
            $results = Inventory::create($input);
        }
        else
        {
            // failure, get errors
            $results = $model->errors;
        }

        
        

        return response()->json($results);
    }    




    /**
     * Display the specified Inventory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $inventory = $this->inventoryRepository->findWithoutFail($id);

        if (empty($inventory)) {
            Flash::error('Inventory not found');

            return redirect(route('inventories.index'));
        }

        return view('inventories.show')->with('inventory', $inventory);
    }

    /**
     * Show the form for editing the specified Inventory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $inventory = $this->inventoryRepository->findWithoutFail($id);

        if (empty($inventory)) {
            Flash::error('Inventory not found');

            return redirect(route('inventories.index'));
        }

        return view('inventories.edit')->with('inventory', $inventory);
    }

    /**
     * Update the specified Inventory in storage.
     *
     * @param  int              $id
     * @param UpdateInventoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInventoryRequest $request)
    {
        $inventory = $this->inventoryRepository->findWithoutFail($id);

        if (empty($inventory)) {
            Flash::error('Inventory not found');

            return redirect(route('inventories.index'));
        }

        $inventory = $this->inventoryRepository->update($request->all(), $id);

        Flash::success('Inventory updated successfully.');

        return redirect(route('inventories.index'));
    }


    public function findByVin($search){
        
            $results = null;
            
            
            if(strlen($search) != 17){
               $search       = str_replace(" ", "%", $search );
               $activitydata = DB::table('view_year_make_model_trim_name')->where('searchOn', 'LIKE', '%'. $search .'%')->paginate(25);
               $results      = $activitydata->all();
               
               
            } else {
               if (preg_match('/[A-HJ-NPR-Z0-9]{13}[0-9]{4}/',$search)){
                
                    $model = new \App\Edmunds;
                    $info = $model->findByVin($search);

                    if($info){
                        
                        $rs['styleID'] = $info['years'][0]['id'];
                        $rs['searchOn'] = $info['years'][0]['year']. ' '.$info['make']['name']. ' '.$info['model']['name']. ' '.$info['years'][0]['styles'][0]['name'];
                        $updates  = array( 'styleid'=> $rs['styleID'] , 'year'=> $info['years'][0]['year'] , 'make'=> $info['make']['name'] , 'model' => $info['model']['name']  );
                        $where    = [ 'styleid'=> $rs['styleID'] ];
                        EdmundMakeModel::updateOrCreate($where ,  $updates );                          
                        $results[] = $rs;
                    }
                
                
               }
            }

            return response()->json($results);
        
    }
    
    public function getVehicleInfo($year , $make = null  , $model = null ){
        

            if($make){
                $activitydata = DB::table('edmunds_make')->where('year', '=', $year )->where('make', 'LIKE', '%'. $make .'%' )->get(['id','styleid','model']);      
            }
            else if($model){
                $activitydata = DB::table('edmunds_make')->where('year', '=', $year )->where('model', 'LIKE', '%'. $model .'%' )->where('make', 'LIKE', '%'. $make .'%' )->get(['id','styleid','model','trim']);                  
            }
            else
                $activitydata = DB::table('edmunds_make')->where('year', '=', $year )->distinct()->get(['make']);
            
            $results = $activitydata->all();
            return response()->json($results);
        
    }    

    


    public function getInventory(){
        
        $activities = new Inventory;
        $activities = $activities->where('createdBy', '=', Auth::user()->id );
        $activities = $activities->paginate();    
        return $activities;
        
    }
    



    /**
     * Remove the specified Inventory from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $inventory = $this->inventoryRepository->findWithoutFail($id);

        if (empty($inventory)) {
            Flash::error('Inventory not found');

            return redirect(route('inventories.index'));
        }

        $this->inventoryRepository->delete($id);

        Flash::success('Inventory deleted successfully.');

        return redirect(route('inventories.index'));
    }
}
