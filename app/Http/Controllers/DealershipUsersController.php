<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDealershipUsersRequest;
use App\Http\Requests\UpdateDealershipUsersRequest;
use App\Repositories\DealershipUsersRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use DB;
use Auth;
use App\Foo;

class DealershipUsersController extends AppBaseController
{
    
    
    /** @var  DealershipUsersRepository */
    private $dealershipUsersRepository;
    
	protected $foo;
	
	public function __construct(Foo $foo , DealershipUsersRepository $dealershipUsersRepo){		
       $this->middleware('auth');	 
	   $this->foo = $foo;
	   $this->dealershipUsersRepository = $dealershipUsersRepo;
	   $this->middleware('permission:users.manage');
    }
	    
    /**
     * Display a listing of the DealershipUsers.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        
        var_dump($request);
        
        $this->dealershipUsersRepository->pushCriteria(new RequestCriteria($request));
        $dealershipUsers = $this->dealershipUsersRepository->all();

        return view('dealership_users.index')->with('dealershipUsers', $dealershipUsers);
    }

    /**
     * Show the form for creating a new DealershipUsers.
     *
     * @return Response
     */
    public function create()
    {
		$country = DB::table('country')->get();
		$roles = DB::table('roles')->get();
   
        return view('dealership_users.create' ,compact('country','roles') );        
    }

    /**
     * Store a newly created DealershipUsers in storage.
     *
     * @param CreateDealershipUsersRequest $request
     *
     * @return Response
     */
    public function store(CreateDealershipUsersRequest $request)
    {
        $data = $request->all();
        $input = null;
        
        try {
            
            DB::beginTransaction();
            //do something
             $savedata = \App\User::create([
    		    'status'     => $data['status'],
                'first_name' => $data['first_name'],
                'last_name'  => $data['last_name'],
                'role'       => 'User',
                'email'      => $data['email'],
                'country'    => $data['country'],
                'username'   => $data['email'],
                'password'   => bcrypt($data['password'])
            ]);

            // echo  Auth::user()->id;
            // DB::enableQueryLog();
            
            $dealership = \App\Models\Dealership::where('createdBy', Auth::user()->id )->get()->first();
            // dd(DB::getQueryLog());

            if(isset($dealership->id)){
                $input = [ 'dealershipid' => $dealership->id , 'userid' => $savedata->id  ];
                $dealershipUsers = $this->dealershipUsersRepository->create($input);
            }

            DB::table('role_user')->insert(['user_id' => $savedata->id , 'role_id' => 2 ]);
    		DB::commit();
    		Flash::success('Employee added.');
            
        } catch (\Illuminate\Database\QueryException $e) {
            
            DB::rollback();
            Flash::success('Employee not added.');

        }  

        
        return redirect(route('dealershipUsers.index'));
    }

    /**
     * Display the specified DealershipUsers.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $dealershipUsers = $this->dealershipUsersRepository->findWithoutFail($id);

        if (empty($dealershipUsers)) {
            Flash::error('Dealership Users not found');

            return redirect(route('dealershipUsers.index'));
        }

        return view('dealership_users.show')->with('dealershipUsers', $dealershipUsers);
    }

    /**
     * Show the form for editing the specified DealershipUsers.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $dealershipUsers = $this->dealershipUsersRepository->findWithoutFail($id);

        if (empty($dealershipUsers)) {
            Flash::error('Dealership Users not found');

            return redirect(route('dealershipUsers.index'));
        }

        return view('dealership_users.edit')->with('dealershipUsers', $dealershipUsers);
    }

    /**
     * Update the specified DealershipUsers in storage.
     *
     * @param  int              $id
     * @param UpdateDealershipUsersRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDealershipUsersRequest $request)
    {
        $dealershipUsers = $this->dealershipUsersRepository->findWithoutFail($id);

        if (empty($dealershipUsers)) {
            Flash::error('Dealership Users not found');

            return redirect(route('dealershipUsers.index'));
        }

        $dealershipUsers = $this->dealershipUsersRepository->update($request->all(), $id);

        Flash::success('Dealership Users updated successfully.');

        return redirect(route('dealershipUsers.index'));
    }

    /**
     * Remove the specified DealershipUsers from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $dealershipUsers = $this->dealershipUsersRepository->findWithoutFail($id);

        if (empty($dealershipUsers)) {
            Flash::error('Dealership Users not found');

            return redirect(route('dealershipUsers.index'));
        }

        $this->dealershipUsersRepository->delete($id);

        Flash::success('Dealership Users deleted successfully.');

        return redirect(route('dealershipUsers.index'));
    }
}
