<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateEdmundMakeModelRequest;
use App\Http\Requests\UpdateEdmundMakeModelRequest;
use App\Repositories\EdmundMakeModelRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class EdmundMakeModelController extends AppBaseController
{
    /** @var  EdmundMakeModelRepository */
    private $edmundMakeModelRepository;

    public function __construct(EdmundMakeModelRepository $edmundMakeModelRepo)
    {
        $this->edmundMakeModelRepository = $edmundMakeModelRepo;
    }

    /**
     * Display a listing of the EdmundMakeModel.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->edmundMakeModelRepository->pushCriteria(new RequestCriteria($request));
        $edmundMakeModels = $this->edmundMakeModelRepository->all();

        return view('edmund_make_models.index')
            ->with('edmundMakeModels', $edmundMakeModels);
    }

    /**
     * Show the form for creating a new EdmundMakeModel.
     *
     * @return Response
     */
    public function create()
    {
        return view('edmund_make_models.create');
    }

    /**
     * Store a newly created EdmundMakeModel in storage.
     *
     * @param CreateEdmundMakeModelRequest $request
     *
     * @return Response
     */
    public function store(CreateEdmundMakeModelRequest $request)
    {
        $input = $request->all();

        $edmundMakeModel = $this->edmundMakeModelRepository->create($input);

        Flash::success('Edmund Make Model saved successfully.');

        return redirect(route('edmundMakeModels.index'));
    }

    /**
     * Display the specified EdmundMakeModel.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $edmundMakeModel = $this->edmundMakeModelRepository->findWithoutFail($id);

        if (empty($edmundMakeModel)) {
            Flash::error('Edmund Make Model not found');

            return redirect(route('edmundMakeModels.index'));
        }

        return view('edmund_make_models.show')->with('edmundMakeModel', $edmundMakeModel);
    }

    /**
     * Show the form for editing the specified EdmundMakeModel.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $edmundMakeModel = $this->edmundMakeModelRepository->findWithoutFail($id);

        if (empty($edmundMakeModel)) {
            Flash::error('Edmund Make Model not found');

            return redirect(route('edmundMakeModels.index'));
        }

        return view('edmund_make_models.edit')->with('edmundMakeModel', $edmundMakeModel);
    }

    /**
     * Update the specified EdmundMakeModel in storage.
     *
     * @param  int              $id
     * @param UpdateEdmundMakeModelRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEdmundMakeModelRequest $request)
    {
        $edmundMakeModel = $this->edmundMakeModelRepository->findWithoutFail($id);

        if (empty($edmundMakeModel)) {
            Flash::error('Edmund Make Model not found');

            return redirect(route('edmundMakeModels.index'));
        }

        $edmundMakeModel = $this->edmundMakeModelRepository->update($request->all(), $id);

        Flash::success('Edmund Make Model updated successfully.');

        return redirect(route('edmundMakeModels.index'));
    }

    /**
     * Remove the specified EdmundMakeModel from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $edmundMakeModel = $this->edmundMakeModelRepository->findWithoutFail($id);

        if (empty($edmundMakeModel)) {
            Flash::error('Edmund Make Model not found');

            return redirect(route('edmundMakeModels.index'));
        }

        $this->edmundMakeModelRepository->delete($id);

        Flash::success('Edmund Make Model deleted successfully.');

        return redirect(route('edmundMakeModels.index'));
    }
}
