<?php

namespace App\Models;

use Auth;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Validator;


/**
 * Class Inventory
 * @package App\Models
 * @version March 31, 2017, 9:49 pm UTC
 */
class Inventory extends Model
{
    use SoftDeletes;

    public $table = 'inventory';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'starred',
        'styleid',
        'tradeinid',
        'vin',
        'notes',
        'mileage',
        'offerprice',
        'purchaseprice',
        'soldprice',
        'tradeinprice',
        'purchasedate',
        'solddate',
        'tradeindate',
        'deliverydate',
        'marketdate',
        'dealershipid',
        'createdBy'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'starred' => 'boolean',
        'styleid' => 'integer',
        'tradeinid' => 'integer',
        'vin' => 'string',
        'notes' => 'string',
        'mileage' => 'integer',
        'offerprice' => 'float',
        'purchaseprice' => 'float',
        'soldprice' => 'float',
        'tradeinprice' => 'float',
        'dealershipid' => 'integer',
        'createdBy' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        //'styleid' => 'required|unique_with:inventory,dealershipid',
        'mileage' => 'required',
        'purchaseprice' => 'required', 

    ];

    public function validate($data)
    {
        
        // make a new validator object
        $validate = Validator::make($data, self::$rules);
        // check for failure
        if ($validate->fails())
        {  // set errors and return false
            $this->errors = $validate->errors();
            return false;
        }

        // validation pass
        return true;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function offersCounters()
    {
        return $this->hasMany(\App\Models\OffersCounter::class);
    }
    
    public function save(array $options = array())
    {
        
        
        if( ! $this->createdBy)
        {
            $this->createdBy = Auth::user()->id;
            $this->dealershipid = Auth::user()->getDealershipId();
        }

        //$this->expires = date("Y-m-d H:i:s" , strtotime('+ '.$this->expires.'days') );

        parent::save($options);
    }

 
    
}
