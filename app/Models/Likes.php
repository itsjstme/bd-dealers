<?php

namespace App\Models;

use Auth;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Validator;


/**
 * Class Inventory
 * @package App\Models
 * @version March 31, 2017, 9:49 pm UTC
 */
class Likes extends Model
{
    use SoftDeletes;

    public $table = 'inventory_likes';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'inventoryid',
        'createdBy'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'inventoryid' => 'integer',
        'createdBy' => 'integer'
    ];


    public function save(array $options = array())
    {
        
        if( ! $this->createdBy)
        {
            $this->createdBy = Auth::user()->id;
        }


        parent::save($options);
    }

 
    
}
