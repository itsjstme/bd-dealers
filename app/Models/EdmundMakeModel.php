<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class EdmundMakeModel
 * @package App\Models
 * @version April 5, 2017, 2:23 pm UTC
 */
class EdmundMakeModel extends Model
{
    use SoftDeletes;

    public $table = 'edmunds_make';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'styleid',
        'make',
        'year',
        'model',
        'trim',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'styleid' => 'integer',
        'make' => 'string',
        'year' => 'integer',
        'model' => 'string',
        'trim' => 'string',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
    
}
