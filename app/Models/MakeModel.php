<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class EdmundMakeModel
 * @package App\Models
 * @version April 5, 2017, 2:23 pm UTC
 */
class MakeModel extends Model
{
    use SoftDeletes;

    public $table = 'make_model';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'styleid',
        'make',
        'model',        
        'body',
        'trim',
        'name',
        'year'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'styleid' => 'integer',
        'make' => 'string',
        'model' => 'string',        
        'body' => 'string',
        'trim' => 'string',
        'name' => 'string',
        'year' => 'integer'        
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
    
}
