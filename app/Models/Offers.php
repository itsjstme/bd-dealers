<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Offers
 * @package App\Models
 * @version March 31, 2017, 10:58 pm UTC
 */
class Offers extends Model
{
    use SoftDeletes;

    public $table = 'offers';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'offerids',
        'title',
        'price',
        'expires',
        'createdBy'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'offerids' => 'string',
        'title' => 'string',
        'price' => 'float',
        'createdBy' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function save(array $options = array())
    {
        
        
        if( ! $this->createdBy)
        {
            $this->createdBy = Auth::user()->id;
        }

        $this->expires = date("Y-m-d H:i:s" , strtotime('+ '.$this->expires.'days') );

        parent::save($options);
    }

    
}
