<?php

namespace App;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use Auth;

/**
 * Class edmunds
 * @package App\Models
 * @version March 17, 2017, 8:54 pm UTC
 */
class Edmunds extends Model
{
    use SoftDeletes;

    public $table = 'edmunds';
    

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'url',
        'results'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'url' => 'string',
        'results' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    private $apiKey = null;

    public function __construct(){
        $this->apiKey  = "rpwhm99xyh8wqnzm2hebqse9";
        $this->results = null;
    }

    private function processAPI($url){

        $this->url     = $url;
        $rs            = DB::table($this->table)->where('url', '=', $this->url  )->get()->first();

        if(isset($rs->results) && count($rs) > 0 ){
            $this->results = $rs->results;    
            
        }else{
			ob_start("ob_gzhandler");
			$this->results = @file_get_contents( $this->url , true);

			if(!empty($this->results)){
	            parent::save(); 
			}                
        }
        
        return $this->results;
        
    }

    
    public function getAllCars(){
        
        $url        = "https://api.edmunds.com/api/vehicle/v2/makes?state=used&view=full&fmt=json&api_key=".$this->apiKey;
        $results = $this->processAPI($url);
		return json_decode($results  ,true);        
        
    }
    

    public function  findByVin($vinNumber){
        
            $url     = "https://api.edmunds.com/api/vehicle/v2/vins/{$vinNumber}?fmt=json&api_key=".$this->apiKey;						
            $results = $this->processAPI($url);
		    return json_decode($results  ,true);            
        
    }


    public function getListCarMakes(){
        
        $url     = "http://api.edmunds.com/api/vehicle/v2/makes?fmt=json&api_key=".$this->apiKey;   
        $results = $this->processAPI($url);
        
		return json_decode($results  ,true);   
        
        
    }
    
    public function getMakeModelDetails($make,$model){
        
        $url     = "http://api.edmunds.com/api/vehicle/v2/{$make}/{$model}?fmt=json&api_key=".$this->apiKey;   
        $results = $this->processAPI($url);
		return json_decode($results  ,true);   
        
        
    }    

    
}
