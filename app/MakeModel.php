<?php

namespace App\Models;


use Auth;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class offers
 * @package App\Models
 * @version March 19, 2017, 8:19 pm UTC
 */
class MakeModel extends Model
{
    use SoftDeletes;

    public $table = 'make_model';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'modelyearID',
        'styleID',
        'make',
        'model',
        'body',
        'trim', 
        'name',
        'year',        
    ];
    
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'modelyearID' => 'integer',
        'styleID' => 'integer',
        'make'  => 'string',
        'model'  => 'string',
        'body'  => 'string',
        'trim'   => 'string',
        'name'  => 'string',
        'year'  => 'integer',      

    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    
}
