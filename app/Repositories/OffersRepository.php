<?php

namespace App\Repositories;

use App\Models\Offers;
use InfyOm\Generator\Common\BaseRepository;

class OffersRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'offerids',
        'title',
        'price',
        'expires',
        'createdBy'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Offers::class;
    }
}
