<?php

namespace App\Repositories;

use App\Models\DealershipUsers;
use InfyOm\Generator\Common\BaseRepository;

class DealershipUsersRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'dealershipid',
        'userid'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DealershipUsers::class;
    }
}
