<?php

namespace App\Repositories;

use App\Models\Inventory;
use InfyOm\Generator\Common\BaseRepository;

class InventoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'starred',
        'styleid',
        'tradeinid',
        'vin',
        'notes',
        'mileage',
        'offerprice',
        'purchaseprice',
        'soldprice',
        'tradeinprice',
        'purchasedate',
        'solddate',
        'tradeindate',
        'deliverydate',
        'marketdate',
        'createdBy'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Inventory::class;
    }
}
