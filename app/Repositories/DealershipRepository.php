<?php

namespace App\Repositories;

use App\Models\Dealership;
use InfyOm\Generator\Common\BaseRepository;

class DealershipRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'address',
        'city',
        'state',
        'zip',
        'phone',
        'createdBy'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Dealership::class;
    }
}
