<?php

namespace App\Repositories;

use App\Models\EdmundMakeModel;
use InfyOm\Generator\Common\BaseRepository;

class EdmundMakeModelRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'styleid',
        'name',
        'year',
        'model',
        'trime'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return EdmundMakeModel::class;
    }
}
