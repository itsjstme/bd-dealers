<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Edmunds;
use App\Models\EdmundMakeModel;


class edmundsAPI extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'edmunds:api {process}';
    

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'process background process using Edmunds API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    private function processAll()
    {

        $attributes = ['styleid','name','year','model'];
        $model = new Edmunds();
        $result = $model->getListCarMakes();

        foreach($result['makes'] as $key => $value){
             
             foreach($value['models'] as $model ){
                 
                 foreach($model['years'] as $years ){
                   $row = [
                        'name' => $value['name'],
                        'year' => $years['year'],
                        'model' => $model['name'],
                    ];                 
                     
                    $model = EdmundMakeModel::updateOrCreate( ['styleid' => $years['id'] ], $row );                     
                     
                 }
             }

        }
        
        return count($row);

    }
    
    private function processStyle(){
        
        $rows = EdmundMakeModel::where('status', 0)->orderBy('id', 'ASC')->take(100)->get();
        $model = new Edmunds();
        
        foreach ($rows as $item) {
            sleep(rand(3, 10));
            $results = $model->getDetails($item->name , $item->model );
            
            if($results){
                
                foreach($results["years"] as $year ){
                    
                    foreach($year["styles"] as $style ){
    
                       $row = [
                                'make' => $item->name,
                                'model'=> $item->model,        
                                'name' => $style["name"] ,
                                'trim' => $style["trim"] ,
                                'year' => $year["year"]
                        ];
                        
                        $save = \App\Models\MakeModel::updateOrCreate( ['styleid' => $style['id'] ], $row ); 
    
                    }
                        
                }
    
               $item->status = 1;
               $item->save();
           
            }
        }
        
    }
    
    private function processAllCars(){
        
        
        $model = new Edmunds();
        $rows  = $model->getAllCars();
        
        foreach ($rows['makes'] as $item) {

            if($item['models']){
                
                
                foreach($item['models'] as $model ){
                    
                    foreach($model["years"] as $year ){
                        
                        if(!empty($item["name"]) && !empty($model["name"]) && !empty($year["year"])){
                               $row = [
                                        'make' => $item["name"],
                                        'model'=> $model["name"],        
                                        'year' => $year["year"]
                                ];
                                

                            $save = \App\Models\EdmundMakeModel::updateOrCreate( ['styleid' => $year['id'] ], $row ); 
                        
                        }
                       
    
                    }
                        
                }
    
           
            }
        }
        
    }    
    
    

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $process = $this->argument('process');
        
        if($process == 'list'){
            $result = $this->processAll();
        }
        
        if($process == 'styles'){
            $result = $this->processStyle();
        }     
        
        if($process == 'cars'){
            $result = $this->processAllCars();
        }
        

        
    }
}
