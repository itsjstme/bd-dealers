#!/bin/bash


sudo add-apt-repository ppa:ondrej/php
sudo apt-get update -y
sudo apt-get install libapache2-mod-php5.6 -y
sudo a2dismod php5
sudo a2enmod php5.6
sudo add-apt-repository ppa:ondrej/php5-compat
sudo apt-get update -y
sudo apt-get dist-upgrade -y
