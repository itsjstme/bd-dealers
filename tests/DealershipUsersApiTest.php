<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DealershipUsersApiTest extends TestCase
{
    use MakeDealershipUsersTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateDealershipUsers()
    {
        $dealershipUsers = $this->fakeDealershipUsersData();
        $this->json('POST', '/api/v1/dealershipUsers', $dealershipUsers);

        $this->assertApiResponse($dealershipUsers);
    }

    /**
     * @test
     */
    public function testReadDealershipUsers()
    {
        $dealershipUsers = $this->makeDealershipUsers();
        $this->json('GET', '/api/v1/dealershipUsers/'.$dealershipUsers->id);

        $this->assertApiResponse($dealershipUsers->toArray());
    }

    /**
     * @test
     */
    public function testUpdateDealershipUsers()
    {
        $dealershipUsers = $this->makeDealershipUsers();
        $editedDealershipUsers = $this->fakeDealershipUsersData();

        $this->json('PUT', '/api/v1/dealershipUsers/'.$dealershipUsers->id, $editedDealershipUsers);

        $this->assertApiResponse($editedDealershipUsers);
    }

    /**
     * @test
     */
    public function testDeleteDealershipUsers()
    {
        $dealershipUsers = $this->makeDealershipUsers();
        $this->json('DELETE', '/api/v1/dealershipUsers/'.$dealershipUsers->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/dealershipUsers/'.$dealershipUsers->id);

        $this->assertResponseStatus(404);
    }
}
