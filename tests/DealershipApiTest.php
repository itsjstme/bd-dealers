<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DealershipApiTest extends TestCase
{
    use MakeDealershipTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateDealership()
    {
        $dealership = $this->fakeDealershipData();
        $this->json('POST', '/api/v1/dealerships', $dealership);

        $this->assertApiResponse($dealership);
    }

    /**
     * @test
     */
    public function testReadDealership()
    {
        $dealership = $this->makeDealership();
        $this->json('GET', '/api/v1/dealerships/'.$dealership->id);

        $this->assertApiResponse($dealership->toArray());
    }

    /**
     * @test
     */
    public function testUpdateDealership()
    {
        $dealership = $this->makeDealership();
        $editedDealership = $this->fakeDealershipData();

        $this->json('PUT', '/api/v1/dealerships/'.$dealership->id, $editedDealership);

        $this->assertApiResponse($editedDealership);
    }

    /**
     * @test
     */
    public function testDeleteDealership()
    {
        $dealership = $this->makeDealership();
        $this->json('DELETE', '/api/v1/dealerships/'.$dealership->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/dealerships/'.$dealership->id);

        $this->assertResponseStatus(404);
    }
}
