<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class OffersApiTest extends TestCase
{
    use MakeOffersTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateOffers()
    {
        $offers = $this->fakeOffersData();
        $this->json('POST', '/api/v1/offers', $offers);

        $this->assertApiResponse($offers);
    }

    /**
     * @test
     */
    public function testReadOffers()
    {
        $offers = $this->makeOffers();
        $this->json('GET', '/api/v1/offers/'.$offers->id);

        $this->assertApiResponse($offers->toArray());
    }

    /**
     * @test
     */
    public function testUpdateOffers()
    {
        $offers = $this->makeOffers();
        $editedOffers = $this->fakeOffersData();

        $this->json('PUT', '/api/v1/offers/'.$offers->id, $editedOffers);

        $this->assertApiResponse($editedOffers);
    }

    /**
     * @test
     */
    public function testDeleteOffers()
    {
        $offers = $this->makeOffers();
        $this->json('DELETE', '/api/v1/offers/'.$offers->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/offers/'.$offers->id);

        $this->assertResponseStatus(404);
    }
}
