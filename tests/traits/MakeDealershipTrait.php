<?php

use Faker\Factory as Faker;
use App\Models\Dealership;
use App\Repositories\DealershipRepository;

trait MakeDealershipTrait
{
    /**
     * Create fake instance of Dealership and save it in database
     *
     * @param array $dealershipFields
     * @return Dealership
     */
    public function makeDealership($dealershipFields = [])
    {
        /** @var DealershipRepository $dealershipRepo */
        $dealershipRepo = App::make(DealershipRepository::class);
        $theme = $this->fakeDealershipData($dealershipFields);
        return $dealershipRepo->create($theme);
    }

    /**
     * Get fake instance of Dealership
     *
     * @param array $dealershipFields
     * @return Dealership
     */
    public function fakeDealership($dealershipFields = [])
    {
        return new Dealership($this->fakeDealershipData($dealershipFields));
    }

    /**
     * Get fake data of Dealership
     *
     * @param array $postFields
     * @return array
     */
    public function fakeDealershipData($dealershipFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->randomDigitNotNull,
            'address' => $fake->word,
            'city' => $fake->word,
            'state' => $fake->word,
            'zip' => $fake->word,
            'phone' => $fake->word,
            'createdBy' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $dealershipFields);
    }
}
