<?php

use Faker\Factory as Faker;
use App\Models\Inventory;
use App\Repositories\InventoryRepository;

trait MakeInventoryTrait
{
    /**
     * Create fake instance of Inventory and save it in database
     *
     * @param array $inventoryFields
     * @return Inventory
     */
    public function makeInventory($inventoryFields = [])
    {
        /** @var InventoryRepository $inventoryRepo */
        $inventoryRepo = App::make(InventoryRepository::class);
        $theme = $this->fakeInventoryData($inventoryFields);
        return $inventoryRepo->create($theme);
    }

    /**
     * Get fake instance of Inventory
     *
     * @param array $inventoryFields
     * @return Inventory
     */
    public function fakeInventory($inventoryFields = [])
    {
        return new Inventory($this->fakeInventoryData($inventoryFields));
    }

    /**
     * Get fake data of Inventory
     *
     * @param array $postFields
     * @return array
     */
    public function fakeInventoryData($inventoryFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'starred' => $fake->word,
            'styleid' => $fake->randomDigitNotNull,
            'tradeinid' => $fake->randomDigitNotNull,
            'vin' => $fake->word,
            'notes' => $fake->word,
            'mileage' => $fake->randomDigitNotNull,
            'offerprice' => $fake->randomDigitNotNull,
            'purchaseprice' => $fake->randomDigitNotNull,
            'soldprice' => $fake->randomDigitNotNull,
            'tradeinprice' => $fake->randomDigitNotNull,
            'purchasedate' => $fake->date('Y-m-d H:i:s'),
            'solddate' => $fake->date('Y-m-d H:i:s'),
            'tradeindate' => $fake->date('Y-m-d H:i:s'),
            'deliverydate' => $fake->date('Y-m-d H:i:s'),
            'marketdate' => $fake->date('Y-m-d H:i:s'),
            'createdBy' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $inventoryFields);
    }
}
