<?php

use Faker\Factory as Faker;
use App\Models\Offers;
use App\Repositories\OffersRepository;

trait MakeOffersTrait
{
    /**
     * Create fake instance of Offers and save it in database
     *
     * @param array $offersFields
     * @return Offers
     */
    public function makeOffers($offersFields = [])
    {
        /** @var OffersRepository $offersRepo */
        $offersRepo = App::make(OffersRepository::class);
        $theme = $this->fakeOffersData($offersFields);
        return $offersRepo->create($theme);
    }

    /**
     * Get fake instance of Offers
     *
     * @param array $offersFields
     * @return Offers
     */
    public function fakeOffers($offersFields = [])
    {
        return new Offers($this->fakeOffersData($offersFields));
    }

    /**
     * Get fake data of Offers
     *
     * @param array $postFields
     * @return array
     */
    public function fakeOffersData($offersFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'offerids' => $fake->word,
            'title' => $fake->word,
            'price' => $fake->randomDigitNotNull,
            'expires' => $fake->date('Y-m-d H:i:s'),
            'createdBy' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $offersFields);
    }
}
