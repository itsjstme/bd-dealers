<?php

use Faker\Factory as Faker;
use App\Models\DealershipUsers;
use App\Repositories\DealershipUsersRepository;

trait MakeDealershipUsersTrait
{
    /**
     * Create fake instance of DealershipUsers and save it in database
     *
     * @param array $dealershipUsersFields
     * @return DealershipUsers
     */
    public function makeDealershipUsers($dealershipUsersFields = [])
    {
        /** @var DealershipUsersRepository $dealershipUsersRepo */
        $dealershipUsersRepo = App::make(DealershipUsersRepository::class);
        $theme = $this->fakeDealershipUsersData($dealershipUsersFields);
        return $dealershipUsersRepo->create($theme);
    }

    /**
     * Get fake instance of DealershipUsers
     *
     * @param array $dealershipUsersFields
     * @return DealershipUsers
     */
    public function fakeDealershipUsers($dealershipUsersFields = [])
    {
        return new DealershipUsers($this->fakeDealershipUsersData($dealershipUsersFields));
    }

    /**
     * Get fake data of DealershipUsers
     *
     * @param array $postFields
     * @return array
     */
    public function fakeDealershipUsersData($dealershipUsersFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'dealershipid' => $fake->randomDigitNotNull,
            'userid' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $dealershipUsersFields);
    }
}
