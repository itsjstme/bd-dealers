<?php

use App\Models\Dealership;
use App\Repositories\DealershipRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DealershipRepositoryTest extends TestCase
{
    use MakeDealershipTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var DealershipRepository
     */
    protected $dealershipRepo;

    public function setUp()
    {
        parent::setUp();
        $this->dealershipRepo = App::make(DealershipRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateDealership()
    {
        $dealership = $this->fakeDealershipData();
        $createdDealership = $this->dealershipRepo->create($dealership);
        $createdDealership = $createdDealership->toArray();
        $this->assertArrayHasKey('id', $createdDealership);
        $this->assertNotNull($createdDealership['id'], 'Created Dealership must have id specified');
        $this->assertNotNull(Dealership::find($createdDealership['id']), 'Dealership with given id must be in DB');
        $this->assertModelData($dealership, $createdDealership);
    }

    /**
     * @test read
     */
    public function testReadDealership()
    {
        $dealership = $this->makeDealership();
        $dbDealership = $this->dealershipRepo->find($dealership->id);
        $dbDealership = $dbDealership->toArray();
        $this->assertModelData($dealership->toArray(), $dbDealership);
    }

    /**
     * @test update
     */
    public function testUpdateDealership()
    {
        $dealership = $this->makeDealership();
        $fakeDealership = $this->fakeDealershipData();
        $updatedDealership = $this->dealershipRepo->update($fakeDealership, $dealership->id);
        $this->assertModelData($fakeDealership, $updatedDealership->toArray());
        $dbDealership = $this->dealershipRepo->find($dealership->id);
        $this->assertModelData($fakeDealership, $dbDealership->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteDealership()
    {
        $dealership = $this->makeDealership();
        $resp = $this->dealershipRepo->delete($dealership->id);
        $this->assertTrue($resp);
        $this->assertNull(Dealership::find($dealership->id), 'Dealership should not exist in DB');
    }
}
