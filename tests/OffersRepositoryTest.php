<?php

use App\Models\Offers;
use App\Repositories\OffersRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class OffersRepositoryTest extends TestCase
{
    use MakeOffersTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var OffersRepository
     */
    protected $offersRepo;

    public function setUp()
    {
        parent::setUp();
        $this->offersRepo = App::make(OffersRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateOffers()
    {
        $offers = $this->fakeOffersData();
        $createdOffers = $this->offersRepo->create($offers);
        $createdOffers = $createdOffers->toArray();
        $this->assertArrayHasKey('id', $createdOffers);
        $this->assertNotNull($createdOffers['id'], 'Created Offers must have id specified');
        $this->assertNotNull(Offers::find($createdOffers['id']), 'Offers with given id must be in DB');
        $this->assertModelData($offers, $createdOffers);
    }

    /**
     * @test read
     */
    public function testReadOffers()
    {
        $offers = $this->makeOffers();
        $dbOffers = $this->offersRepo->find($offers->id);
        $dbOffers = $dbOffers->toArray();
        $this->assertModelData($offers->toArray(), $dbOffers);
    }

    /**
     * @test update
     */
    public function testUpdateOffers()
    {
        $offers = $this->makeOffers();
        $fakeOffers = $this->fakeOffersData();
        $updatedOffers = $this->offersRepo->update($fakeOffers, $offers->id);
        $this->assertModelData($fakeOffers, $updatedOffers->toArray());
        $dbOffers = $this->offersRepo->find($offers->id);
        $this->assertModelData($fakeOffers, $dbOffers->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteOffers()
    {
        $offers = $this->makeOffers();
        $resp = $this->offersRepo->delete($offers->id);
        $this->assertTrue($resp);
        $this->assertNull(Offers::find($offers->id), 'Offers should not exist in DB');
    }
}
