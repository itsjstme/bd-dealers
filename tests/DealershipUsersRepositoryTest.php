<?php

use App\Models\DealershipUsers;
use App\Repositories\DealershipUsersRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DealershipUsersRepositoryTest extends TestCase
{
    use MakeDealershipUsersTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var DealershipUsersRepository
     */
    protected $dealershipUsersRepo;

    public function setUp()
    {
        parent::setUp();
        $this->dealershipUsersRepo = App::make(DealershipUsersRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateDealershipUsers()
    {
        $dealershipUsers = $this->fakeDealershipUsersData();
        $createdDealershipUsers = $this->dealershipUsersRepo->create($dealershipUsers);
        $createdDealershipUsers = $createdDealershipUsers->toArray();
        $this->assertArrayHasKey('id', $createdDealershipUsers);
        $this->assertNotNull($createdDealershipUsers['id'], 'Created DealershipUsers must have id specified');
        $this->assertNotNull(DealershipUsers::find($createdDealershipUsers['id']), 'DealershipUsers with given id must be in DB');
        $this->assertModelData($dealershipUsers, $createdDealershipUsers);
    }

    /**
     * @test read
     */
    public function testReadDealershipUsers()
    {
        $dealershipUsers = $this->makeDealershipUsers();
        $dbDealershipUsers = $this->dealershipUsersRepo->find($dealershipUsers->id);
        $dbDealershipUsers = $dbDealershipUsers->toArray();
        $this->assertModelData($dealershipUsers->toArray(), $dbDealershipUsers);
    }

    /**
     * @test update
     */
    public function testUpdateDealershipUsers()
    {
        $dealershipUsers = $this->makeDealershipUsers();
        $fakeDealershipUsers = $this->fakeDealershipUsersData();
        $updatedDealershipUsers = $this->dealershipUsersRepo->update($fakeDealershipUsers, $dealershipUsers->id);
        $this->assertModelData($fakeDealershipUsers, $updatedDealershipUsers->toArray());
        $dbDealershipUsers = $this->dealershipUsersRepo->find($dealershipUsers->id);
        $this->assertModelData($fakeDealershipUsers, $dbDealershipUsers->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteDealershipUsers()
    {
        $dealershipUsers = $this->makeDealershipUsers();
        $resp = $this->dealershipUsersRepo->delete($dealershipUsers->id);
        $this->assertTrue($resp);
        $this->assertNull(DealershipUsers::find($dealershipUsers->id), 'DealershipUsers should not exist in DB');
    }
}
