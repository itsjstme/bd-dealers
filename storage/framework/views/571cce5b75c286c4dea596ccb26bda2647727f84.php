<table class="table table-responsive" id="offers-table">
    <thead>
        <th>Offerids</th>
        <th>Title</th>
        <th>Price</th>
        <th>Expires</th>
        <th>Createdby</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $offers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $offers): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $offers->offerids; ?></td>
            <td><?php echo $offers->title; ?></td>
            <td><?php echo $offers->price; ?></td>
            <td><?php echo $offers->expires; ?></td>
            <td><?php echo $offers->createdBy; ?></td>
            <td>
                <?php echo Form::open(['route' => ['offers.destroy', $offers->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('offers.show', [$offers->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('offers.edit', [$offers->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>