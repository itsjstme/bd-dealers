<table class="table table-responsive" id="dealershipUsers-table">
    <thead>
        <th>Dealershipid</th>
        <th>Userid</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $dealershipUsers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dealershipUsers): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $dealershipUsers->dealershipid; ?></td>
            <td><?php echo $dealershipUsers->userid; ?></td>
            <td>
                <?php echo Form::open(['route' => ['dealershipUsers.destroy', $dealershipUsers->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('dealershipUsers.show', [$dealershipUsers->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('dealershipUsers.edit', [$dealershipUsers->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>