<?php $__env->startSection('header'); ?>
<link rel="stylesheet" href="<?php echo e(URL::to('/')); ?>/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
<link rel="stylesheet" href="<?php echo e(URL::to('/')); ?>/global/vendor/bootstrap-maxlength/bootstrap-maxlength.css">
<link rel="stylesheet" href="<?php echo e(URL::to('/')); ?>/global/vendor/jt-timepicker/jquery-timepicker.css">
<link rel="stylesheet" href="<?php echo e(URL::to('/')); ?>/assets/examples/css/forms/advanced.css">
<link rel="stylesheet" href="<?php echo e(URL::to('/')); ?>/angular-ui-grid/ui-grid.min.css">
    
<?php $__env->stopSection(); ?>



<?php $__env->startSection('content'); ?> 



<style>
p.redcolor{color:red; font-size:16px;}
.spancolor{color:red;}
.help-block{color:red;}
</style>

<div class="page-header">
  <h1 class="page-title font_lato">Create a New Offer</h1>
  <div class="page-header-actions">
  <ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('/dashboard')); ?>"><?php echo e(trans('app.home')); ?></a></li>
		<li><a href="<?php echo e(URL::to('offers')); ?>"><?php echo e(trans('app.offers')); ?></a></li>
		<li class="active"><?php echo e(trans('app.create')); ?></li>
	</ol>
  </div>
</div>
	
<div class="page-content" ng-app="app" ng-cloak>	
<div class="panel">
<div class="panel-body container-fluid">
<!------------------------start insert, update, delete message  ---------------->
<div class="row">
<?php if(session('msg_success')): ?>
	<div class="alert dark alert-icon alert-success alert-dismissible alertDismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">×</span>
	  </button>
	  <i class="icon wb-check" aria-hidden="true"></i> 
	  <?php echo e(session('msg_success')); ?>

	</div>
<?php endif; ?>
<?php if(session('msg_update')): ?>
	<div class="alert dark alert-icon alert-info alert-dismissible alertDismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">×</span>
	  </button>
	  <i class="icon wb-check" aria-hidden="true"></i> 
	  <?php echo e(session('msg_update')); ?>

	</div>
<?php endif; ?>
<?php if(session('msg_delete')): ?>
	<div class="alert dark alert-icon alert-danger alert-dismissible alertDismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">×</span>
	  </button>
	  <i class="icon wb-check" aria-hidden="true"></i> 
	  <?php echo e(session('msg_delete')); ?>

	</div>
<?php endif; ?>
</div>
<form  name="userForm" action="<?php echo e(URL::to('store')); ?>" ng-submit="submitForm(userForm.$valid)" novalidate  id="demo-form2" data-parsley-validate="" method="post" novalidate="">
		<?php echo e(csrf_field()); ?>

<div class="row row-lg">
	<div class="col-sm-8" style="border-right: 1px dotted #ddd;">
	  <!-- Example Basic Form -->	              
			<div class="row">
				
			<div class="col-sm-12">
				<input type="hidden"  id="title" ng-model="offerids" name="offerids" ng-init="offerids='<?php echo e(old('offerids')); ?>'" required>
			</div>
			  <div class="form-group col-sm-4">
				<label class="control-label" for="inputBasicFirstName">Title<span class="spancolor">*</span> </label>
				<input type="text" class="form-control" id="title" ng-model="title" name="title" ng-init="title='<?php echo e(old('title')); ?>'" placeholder="Enter title" required>
			  </div>
			  <div class="form-group col-sm-3">
				<label class="control-label" for="inputBasicFirstName">Expires</label>
				<select ng-model="expires"  class="form-control" name="expires" required ng-init="expires = '<?php echo e(old('expires')); ?>'">
					<option value="">Select days</option>	
					<option value="10">10 days</option>
					<option value="13">13 days</option>
					<option value="15">15 days</option>
					<option value="19">19 days</option>
					<option value="22">22 days</option>
					<option value="30">30 days</option>					
				</select>				
			  </div>			  
			  <div class="form-group col-sm-2">
				<label class="control-label" for="inputBasicLastName">Price<span class="spancolor">*</span> </label>
				<input type="text" class="form-control" id="offerprice" name="offerprice" ng-model="offerprice" ng-init="offerprice='<?php echo e(old('offerprice')); ?>'" placeholder="Enter Price" required data-plugin="formatter"
                    data-pattern="$[[999]],[[999]],[[999]].[[99]]" />
                    <p class="help-block">$999,999,999.99</p>
			  </div>			  
			  <div class="form-group col-sm-2">
				<label class="control-label" for="inputBasicLastName">Lowest Price<span class="spancolor">*</span> </label>
				<input type="text" class="form-control" id="lowestprice" name="lowestprice" ng-model="lowestprice" ng-init="lowestprice='<?php echo e(old('lowestprice')); ?>'" placeholder="Enter Price" required data-plugin="formatter"
                    data-pattern="$[[999]],[[999]],[[999]].[[99]]" />
                    <p class="help-block">$999,999,999.99</p>
			  </div>
			  <div class="form-group col-sm-12">
			  	<p>Only Vehicles not alreadybeing offered will show up.</p>
				<label class="control-label" for="inputBasicFirstName">Search Inventory<span class="spancolor">*</span> </label>
				<div class="input-group">
                    <input type="text" class="form-control" id="vin" ng-model="vinNumber" name="vin" ng-init="vin='<?php echo e(old('vin')); ?>'" placeholder="Search By <?php echo e(trans('app.vin')); ?>" required>
                    <span class="input-group-btn">
                      <button id="vinNumberSearch"  class="btn btn-primary"><i class="icon wb-search" aria-hidden="true"></i></button>
                    </span>
                  </div>				
			  </div>		  
			</div>
			<div class="row">
					<div class="form-group col-sm-6">
						<button type="submit"  class="btn btn-primary ladda-button">
						<i class="fa fa-save"></i>  Save Offer
						</button>
					</div>	
			</div>
	</div>
	
	<div class="row">
			<div class="col-sm-4">
		 <div class="widget widget-shadow">
            <div class="widget-header bg-blue-600 white padding-15 clearfix">
              <a class="avatar avatar-lg pull-left margin-right-20" href="javascript:void(0)">
                <img src="http://crud-itsjstme.c9users.io/global/portraits/16.jpg" alt="">
              </a>
              <div class="font-size-18" ng-bind="title"></div>
              <div class="grey-300 font-size-14" ng-bind="price"></div>
              <div class="grey-300 font-size-14">expires in <span ng-bind="expires"></span> days</div>
            </div>
            <div class="widget-content">
              <ul class="list-group list-group-bordered">
                <li class="list-group-item">
                  <span class="badge badge-success">84K</span>
                  <i class="icon wb-inbox" aria-hidden="true" draggable="true"></i> BMW 325i 
                </li>
              </ul>
            </div>
          </div>
					
			</div>
	</div>
  </div>
</form> 
</div>
<!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
</div><br/>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>