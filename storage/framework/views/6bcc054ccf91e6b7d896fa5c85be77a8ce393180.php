
<?php $__env->startSection('content'); ?> 
<link rel="stylesheet" href="<?php echo e(URL::to('/')); ?>/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
<link rel="stylesheet" href="<?php echo e(URL::to('/')); ?>/global/vendor/bootstrap-maxlength/bootstrap-maxlength.css">
<link rel="stylesheet" href="<?php echo e(URL::to('/')); ?>/global/vendor/jt-timepicker/jquery-timepicker.css">
<link rel="stylesheet" href="<?php echo e(URL::to('/')); ?>/assets/examples/css/forms/advanced.css">

<style>
p.redcolor{color:red; font-size:16px;}
.spancolor{color:red;}
.help-block{color:red;}
</style>

<div class="page-header">
  <h1 class="page-title font_lato">Create New Customer</h1>
  <div class="page-header-actions">
  <ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('/dashboard')); ?>"><?php echo e(trans('app.home')); ?></a></li>
		<li><a href="<?php echo e(URL::to('userlist')); ?>"><?php echo e(trans('app.users')); ?></a></li>
		<li class="active"><?php echo e(trans('app.create')); ?></li>
	</ol>
  </div>
</div>
	
<div class="page-content" ng-app="app" ng-cloak>	
<div class="panel">
<div class="panel-body container-fluid">
<!------------------------start insert, update, delete message  ---------------->
<div class="row">
<?php if(session('msg_success')): ?>
	<div class="alert dark alert-icon alert-success alert-dismissible alertDismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">×</span>
	  </button>
	  <i class="icon wb-check" aria-hidden="true"></i> 
	  <?php echo e(session('msg_success')); ?>

	</div>
<?php endif; ?>
<?php if(session('msg_update')): ?>
	<div class="alert dark alert-icon alert-info alert-dismissible alertDismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">×</span>
	  </button>
	  <i class="icon wb-check" aria-hidden="true"></i> 
	  <?php echo e(session('msg_update')); ?>

	</div>
<?php endif; ?>
<?php if(session('msg_delete')): ?>
	<div class="alert dark alert-icon alert-danger alert-dismissible alertDismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">×</span>
	  </button>
	  <i class="icon wb-check" aria-hidden="true"></i> 
	  <?php echo e(session('msg_delete')); ?>

	</div>
<?php endif; ?>
</div>
<form  name="userForm" action="<?php echo e(URL::to('/customer/registration')); ?>" ng-submit="submitForm(userForm.$valid)" novalidate  id="demo-form2" data-parsley-validate="" method="post" novalidate="">
		<?php echo e(csrf_field()); ?>

<div class="row row-lg">
	<div class="col-sm-8" style="border-right: 1px dotted #ddd;">
	  <!-- Example Basic Form -->	              
			<div class="row">
			  <div class="form-group col-sm-6">
				<label class="control-label" for="inputBasicFirstName">Dealership<span class="spancolor">*</span> </label>
				<input type="text" class="form-control" id="dealershipname" ng-model="dealershipname" name="dealershipname" ng-init="dealershipname='<?php echo e(old('dealershipname')); ?>'" placeholder="Dealership" required>
				 <?php if($errors->has('dealershipname')): ?>
				  <span class="help-block">
				   <strong><?php echo e($errors->first('dealershipname')); ?></strong>
				  </span>
				 <?php endif; ?>				
			  </div>
			  <div class="form-group col-sm-3">
				<label class="control-label" for="inputBasicFirstName"><?php echo e(trans('app.first_name')); ?><span class="spancolor">*</span> </label>
				<input type="text" class="form-control" id="first_name" ng-model="first_name" name="first_name" ng-init="first_name='<?php echo e(old('first_name')); ?>'" placeholder="<?php echo e(trans('app.first_name')); ?>" required>
				 <?php if($errors->has('first_name')): ?>
				  <span class="help-block">
				   <strong><?php echo e($errors->first('first_name')); ?></strong>
				  </span>
				 <?php endif; ?>					
			  </div>
			  <div class="form-group col-sm-3">
				<label class="control-label" for="inputBasicLastName"><?php echo e(trans('app.last_name')); ?><span class="spancolor">*</span> </label>
				<input type="text" class="form-control" id="last_name" name="last_name" ng-model="last_name" ng-init="last_name='<?php echo e(old('last_name')); ?>'" placeholder="<?php echo e(trans('app.last_name')); ?>" required>
				 <?php if($errors->has('last_name')): ?>
				  <span class="help-block">
				   <strong><?php echo e($errors->first('last_name')); ?></strong>
				  </span>
				 <?php endif; ?>					
			  </div>
			  <div class="form-group col-sm-4">
				<label class="control-label" for="inputBasicFirstName">Credit Card Number<span class="spancolor">*</span> </label>
				<input class="form-control" id="number" ng-model="number" name="number" ng-init="number='<?php echo e(old('number')); ?>'" data-plugin="formatter" data-pattern="[[9999]]-[[9999]]-[[9999]]-[[9999]]" placeholder="Dealership" type="text" required>
				 <?php if($errors->has('number')): ?>
				  <span class="help-block">
				   <strong><?php echo e($errors->first('number')); ?></strong>
				  </span>
				 <?php endif; ?>	
			  </div>
			  <div class="form-group col-sm-3">
				<label class="control-label" for="inputBasicFirstName">Expiration Month<span class="spancolor">*</span> </label>
					<select class="form-control" id="exp_month" ng-model="exp_month" name="exp_month" ng-init="exp_month='<?php echo e(old('exp_month')); ?>'" require>
						    <option selected value=''>--Select Month--</option>
						    <option value='1'>Janaury</option>
						    <option value='2'>February</option>
						    <option value='3'>March</option>
						    <option value='4'>April</option>
						    <option value='5'>May</option>
						    <option value='6'>June</option>
						    <option value='7'>July</option>
						    <option value='8'>August</option>
						    <option value='9'>September</option>
						    <option value='10'>October</option>
						    <option value='11'>November</option>
						    <option value='12'>December</option>
	                  </select>	
				 <?php if($errors->has('exp_month')): ?>
				  <span class="help-block">
				   <strong><?php echo e($errors->first('exp_month')); ?></strong>
				  </span>
				 <?php endif; ?>		                  
			  </div>			  
			  <div class="form-group col-sm-3">
				<label class="control-label" for="inputBasicFirstName">Expiration Year<span class="spancolor">*</span> </label>
				<input type="text" class="form-control" id="exp_year" ng-model="exp_year" name="exp_year" ng-init="exp_year='<?php echo e(old('exp_year')); ?>'" placeholder="Enter Year" required>
				 <?php if($errors->has('exp_year')): ?>
				  <span class="help-block">
				   <strong><?php echo e($errors->first('exp_year')); ?></strong>
				  </span>
				 <?php endif; ?>					
			  </div>
			  <div class="form-group col-sm-2">
				<label class="control-label" for="inputBasicLastName">CCV<span class="spancolor">*</span> </label>
				<input type="text" class="form-control" id="exp_ccv" name="exp_ccv" ng-model="exp_ccv" ng-init="exp_ccv='<?php echo e(old('exp_ccv')); ?>'" placeholder="CCV" required>
				 <?php if($errors->has('exp_ccv')): ?>
				  <span class="help-block">
				   <strong><?php echo e($errors->first('exp_ccv')); ?></strong>
				  </span>
				 <?php endif; ?>					
			  </div>
				<div class="form-group col-sm-12">
				<label class="control-label" for="inputBasicFirstName">Billing Address<span class="spancolor">*</span></label>
				<textarea class="form-control" id="billingaddress" name="billingaddress" value="<?php echo e(old('billingaddress')); ?>" placeholder="Billing Address" autocomplete="off" required></textarea>
				 <?php if($errors->has('billingaddress')): ?>
				  <span class="help-block">
				   <strong><?php echo e($errors->first('billingaddress')); ?></strong>
				  </span>
				 <?php endif; ?>					
			 	</div>			  
			</div>
			
			<div class="row">			  
			  <div class="form-group col-sm-4">
				<label class="control-label" for="inputBasicFirstName"><?php echo e(trans('app.phone')); ?><span class="spancolor">*</span></label>
				<input type="text" class="form-control" id="phone" name="phone" placeholder="<?php echo e(trans('app.phone')); ?>" value="<?php echo e(old('phone')); ?>" autocomplete="off" data-plugin="formatter" data-pattern="([[999]]) [[999]]-[[9999]]" required>
				 <?php if($errors->has('phone')): ?>
				  <span class="help-block">
				   <strong><?php echo e($errors->first('phone')); ?></strong>
				  </span>
				 <?php endif; ?>					
			  </div>			  
			  <div class="form-group col-sm-4">			  
			  <label class="control-label" for="inputBasicFirstName"><?php echo e(trans('app.date_of_birth')); ?></label>
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="icon wb-calendar" aria-hidden="true"></i>
                    </span>
                    <input type="text" class="form-control" name="date_of_birth" value="<?php echo e(old('date_of_birth')); ?>" placeholder="<?php echo e(trans('app.date_of_birth')); ?>" data-plugin="datepicker">
                  </div>
                </div>			  
			  <div class="form-group col-sm-4">
				<label class="control-label"><?php echo e(trans('app.country')); ?> <span class="spancolor">*</span></label>
				<select ng-model="country"  class="form-control" name="country" required ng-init="country = '<?php echo e(old('country')); ?>'">
					<option value=""><?php echo e(trans('app.select_country')); ?> </option>	
					<?php $__currentLoopData = $country; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<option value="<?php echo e($data->nicename); ?>"><?php echo e($data->nicename); ?></option>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</select>
				 <?php if($errors->has('select_country')): ?>
				  <span class="help-block">
				   <strong><?php echo e($errors->first('select_country')); ?></strong>
				  </span>
				 <?php endif; ?>					
			  </div>			  
			</div>
			<div class="row">
				<div class="form-group col-sm-12">
				<label class="control-label" for="inputBasicFirstName">Notes</label>
				<textarea class="form-control" id="notes" name="notes" value="<?php echo e(old('notes')); ?>" placeholder="Notes" autocomplete="off"></textarea>
				<!--<input type="text" class="form-control" id="address" name="address" value="<?php echo e(old('address')); ?>" placeholder="<?php echo e(trans('app.address')); ?>" autocomplete="off">-->
			 	</div>
			  
			</div>
	  
	</div>
	<div class="col-sm-4">
	  <!-- Example Basic Form Without Label -->
		<div class="form-group">
		  <label class="control-label" for="inputBasicEmail"><?php echo e(trans('app.email_address')); ?> <span class="spancolor">*</span></label>
		  <input type="email" class="form-control" ng-model="email" ng-init="email='<?php echo e(old('email')); ?>'" required id="email" name="email" placeholder="<?php echo e(trans('app.email_address')); ?>" autocomplete="off">
		 <?php if($errors->has('email')): ?>
		  <span class="help-block">
		   <strong><?php echo e($errors->first('email')); ?></strong>
		  </span>
		 <?php endif; ?>
		</div>
		<div class="form-group">
		<label class="control-label" for="inputBasicPassword"> <?php echo e(trans('app.password')); ?> <span class="spancolor">*</span></label>
			<input type="password" class="form-control"  ng-model="password" placeholder="<?php echo e(trans('app.password')); ?>" name="password" required="required" ng-init="password = '<?php echo e(old('password')); ?>'" ng-minlength="6"/>
				 <?php if($errors->has('password')): ?>
				  <span class="help-block">
				   <strong><?php echo e($errors->first('password')); ?></strong>
				  </span>
				 <?php endif; ?>	
		</div>
		<div class="form-group">
		<label class="control-label" for="inputBasicPassword"><?php echo e(trans('app.confirm_password')); ?> <span class="spancolor">*</span></label>
			<input type="password" class="form-control"  name="confirm_password" ng-model="confirm_password" ng-init="confirm_password = '<?php echo e(old('confirm_password')); ?>'" placeholder="<?php echo e(trans('app.confirm_password')); ?>" match-password="password" required >
				 <?php if($errors->has('confirm_password')): ?>
				  <span class="help-block">
				   <strong><?php echo e($errors->first('confirm_password')); ?></strong>
				  </span>
				 <?php endif; ?>	
		</div>
			
	</div>
	<div style="clear:both"></div>
	<div class="form-group col-sm-6">
		<button type="submit" ng-disabled="userForm.$invalid" class="btn btn-primary ladda-button" data-plugin="ladda" data-style="expand-left">
			<i class="fa fa-save"></i>  <?php echo e(trans('app.create_an_account')); ?>

		<span class="ladda-spinner"></span><div class="ladda-progress" style="width: 0px;"></div>
		</button>
	
	</div>
  </div>
</form> 
</div>
<!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
</div><br/>
		
<script>
var app = angular.module('app', []);

app.directive("matchPassword", function () {
    return {
        require: "ngModel",
        scope: {
            otherModelValue: "=matchPassword"
        },
        link: function(scope, element, attributes, ngModel) {

            ngModel.$validators.matchPassword = function(modelValue) {
                return modelValue == scope.otherModelValue;
            };

            scope.$watch("otherModelValue", function() {
                ngModel.$validate();
            });
        }
    };
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>