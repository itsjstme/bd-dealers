<!-- Starred Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('starred', 'Starred:'); ?>

    <label class="checkbox-inline">
        <?php echo Form::hidden('starred', false); ?>

        <?php echo Form::checkbox('starred', '1', null); ?> 1
    </label>
</div>

<!-- Styleid Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('styleid', 'Styleid:'); ?>

    <?php echo Form::number('styleid', null, ['class' => 'form-control']); ?>

</div>

<!-- Tradeinid Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('tradeinid', 'Tradeinid:'); ?>

    <?php echo Form::number('tradeinid', null, ['class' => 'form-control']); ?>

</div>

<!-- Vin Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('vin', 'Vin:'); ?>

    <?php echo Form::text('vin', null, ['class' => 'form-control']); ?>

</div>

<!-- Notes Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('notes', 'Notes:'); ?>

    <?php echo Form::text('notes', null, ['class' => 'form-control']); ?>

</div>

<!-- Mileage Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('mileage', 'Mileage:'); ?>

    <?php echo Form::number('mileage', null, ['class' => 'form-control']); ?>

</div>

<!-- Offerprice Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('offerprice', 'Offerprice:'); ?>

    <?php echo Form::number('offerprice', null, ['class' => 'form-control']); ?>

</div>

<!-- Purchaseprice Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('purchaseprice', 'Purchaseprice:'); ?>

    <?php echo Form::number('purchaseprice', null, ['class' => 'form-control']); ?>

</div>

<!-- Soldprice Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('soldprice', 'Soldprice:'); ?>

    <?php echo Form::number('soldprice', null, ['class' => 'form-control']); ?>

</div>

<!-- Tradeinprice Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('tradeinprice', 'Tradeinprice:'); ?>

    <?php echo Form::number('tradeinprice', null, ['class' => 'form-control']); ?>

</div>

<!-- Purchasedate Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('purchasedate', 'Purchasedate:'); ?>

    <?php echo Form::date('purchasedate', null, ['class' => 'form-control']); ?>

</div>

<!-- Solddate Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('solddate', 'Solddate:'); ?>

    <?php echo Form::date('solddate', null, ['class' => 'form-control']); ?>

</div>

<!-- Tradeindate Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('tradeindate', 'Tradeindate:'); ?>

    <?php echo Form::date('tradeindate', null, ['class' => 'form-control']); ?>

</div>

<!-- Deliverydate Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('deliverydate', 'Deliverydate:'); ?>

    <?php echo Form::date('deliverydate', null, ['class' => 'form-control']); ?>

</div>

<!-- Marketdate Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('marketdate', 'Marketdate:'); ?>

    <?php echo Form::date('marketdate', null, ['class' => 'form-control']); ?>

</div>

<!-- Createdby Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('createdBy', 'Createdby:'); ?>

    <?php echo Form::number('createdBy', null, ['class' => 'form-control']); ?>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo route('inventories.index'); ?>" class="btn btn-default">Cancel</a>
</div>
