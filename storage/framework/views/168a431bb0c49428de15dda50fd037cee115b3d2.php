<?php $__env->startSection('header'); ?>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular-touch.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular-animate.js"></script>
    <script src="http://ui-grid.info/docs/grunt-scripts/csv.js"></script>
    <script src="http://ui-grid.info/docs/grunt-scripts/pdfmake.js"></script>
    <script src="http://ui-grid.info/docs/grunt-scripts/vfs_fonts.js"></script>
    <script src="/angular-ui-grid/ui-grid.js"></script>
    <script src="/angular-ui-grid/ui-grid.css"></script>
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
   <script src="/js/inventory.js"></script>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?> 
<link rel="stylesheet" href="<?php echo e(URL::to('/')); ?>/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
<link rel="stylesheet" href="<?php echo e(URL::to('/')); ?>/global/vendor/bootstrap-maxlength/bootstrap-maxlength.css">
<link rel="stylesheet" href="<?php echo e(URL::to('/')); ?>/global/vendor/jt-timepicker/jquery-timepicker.css">
<link rel="stylesheet" href="<?php echo e(URL::to('/')); ?>/assets/examples/css/forms/advanced.css">

<style>
p.redcolor{color:red; font-size:16px;}
.spancolor{color:red;}
.help-block{color:red;}
</style>

<div class="page-header">
  <h1 class="page-title font_lato"><?php echo e(trans('app.create_new_car')); ?></h1>
  <div class="page-header-actions">
  <ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('/dashboard')); ?>"><?php echo e(trans('app.home')); ?></a></li>
		<li><a href="<?php echo e(URL::to('userlist')); ?>"><?php echo e(trans('app.users')); ?></a></li>
		<li class="active"><?php echo e(trans('app.create')); ?></li>
	</ol>
  </div>
</div>
	
<div class="page-content" ng-app="app" ng-cloak>	
<div class="panel">
<div class="panel-body container-fluid">
<!------------------------start insert, update, delete message  ---------------->
<div class="row">
<?php if(session('msg_success')): ?>
	<div class="alert dark alert-icon alert-success alert-dismissible alertDismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">×</span>
	  </button>
	  <i class="icon wb-check" aria-hidden="true"></i> 
	  <?php echo e(session('msg_success')); ?>

	</div>
<?php endif; ?>
<?php if(session('msg_update')): ?>
	<div class="alert dark alert-icon alert-info alert-dismissible alertDismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">×</span>
	  </button>
	  <i class="icon wb-check" aria-hidden="true"></i> 
	  <?php echo e(session('msg_update')); ?>

	</div>
<?php endif; ?>
<?php if(session('msg_delete')): ?>
	<div class="alert dark alert-icon alert-danger alert-dismissible alertDismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">×</span>
	  </button>
	  <i class="icon wb-check" aria-hidden="true"></i> 
	  <?php echo e(session('msg_delete')); ?>

	</div>
<?php endif; ?>
</div>
<form  name="userForm" action="<?php echo e(URL::to('store')); ?>" ng-submit="submitForm(userForm.$valid)" novalidate  id="demo-form2" data-parsley-validate="" method="post" novalidate="">
		<?php echo e(csrf_field()); ?>

<div class="row row-lg">
	<div class="col-sm-8" style="border-right: 1px dotted #ddd;">
	  <!-- Example Basic Form -->	              
			<div class="row">
			<div class="col-sm-12">
			</div>
			  <div class="form-group col-sm-8">
				<label class="control-label" for="inputBasicFirstName"><?php echo e(trans('app.vin')); ?><span class="spancolor">*</span> </label>
				<div class="input-group">
                    <input type="text" class="form-control" id="vin" ng-model="vinNumber" name="vin" ng-init="vin='<?php echo e(old('vin')); ?>'" placeholder="Search By <?php echo e(trans('app.vin')); ?>" required>
                    <span class="input-group-btn">
                      <button id="vinNumberSearch"  class="btn btn-primary"><i class="icon wb-search" aria-hidden="true"></i></button>
                    </span>
                  </div>				
			  </div>			
			  <div class="form-group col-sm-4">
				    <label class="control-label" for="inputBasicLastName"><?php echo e(trans('app.mileage')); ?><span class="spancolor">*</span> </label>
				    <input type="text" class="form-control" id="mileage" name="mileage" ng-model="mileage" ng-init="mileage='<?php echo e(old('mileage')); ?>'" placeholder="<?php echo e(trans('app.mileage')); ?>" required>			  	
			  </div>
		</div>
		<div class="row">
			  <div class="form-group col-sm-3">
				<label class="control-label" for="inputBasicFirstName"><?php echo e(trans('app.year')); ?><span class="spancolor">*</span> </label>
				<input type="text" class="form-control" id="year" ng-model="year" name="year" ng-init="year='<?php echo e(old('year')); ?>'" placeholder="<?php echo e(trans('app.year')); ?>" required>
			  </div>			
			  <div class="form-group col-sm-3">
				<label class="control-label" for="inputBasicFirstName"><?php echo e(trans('app.make')); ?><span class="spancolor">*</span> </label>
				<input type="text" class="form-control" id="make" ng-model="make" name="make" ng-init="make='<?php echo e(old('make')); ?>'" placeholder="<?php echo e(trans('app.make')); ?>" required>
			  </div>
			  <div class="form-group col-sm-3">
				<label class="control-label" for="inputBasicLastName"><?php echo e(trans('app.model')); ?><span class="spancolor">*</span> </label>
				<input type="text" class="form-control" id="model" name="model" ng-model="model" ng-init="model='<?php echo e(old('model')); ?>'" placeholder="<?php echo e(trans('app.model')); ?>" required>
			  </div>
			  <div class="form-group col-sm-3">
				<label class="control-label" for="inputBasicLastName"><?php echo e(trans('app.mileage')); ?><span class="spancolor">*</span> </label>
				<input type="text" class="form-control" id="mileage" name="mileage" ng-model="mileage" ng-init="mileage='<?php echo e(old('mileage')); ?>'" placeholder="<?php echo e(trans('app.mileage')); ?>" required>
			  </div>			  
		</div>
		<div class="row">
		    
                <div class="nav-tabs-horizontal">
                  <ul class="nav nav-tabs nav-tabs-solid" data-plugin="nav-tabs" role="tablist">
                    <li class="active" role="presentation"><a data-toggle="tab" href="#exampleTabsSolidOne" aria-controls="exampleTabsSolidOne" role="tab" aria-expanded="false">Purchase Information</a></li>
                    <li role="presentation" class=""><a data-toggle="tab" href="#exampleTabsSolidTwo" aria-controls="exampleTabsSolidTwo" role="tab" aria-expanded="false">Sales Information</a></li>
                    <li role="presentation" class=""><a data-toggle="tab" href="#exampleTabsSolidThree" aria-controls="exampleTabsSolidThree" role="tab" aria-expanded="true">TradeIn Information</a></li>
                    <li role="presentation" class=""><a data-toggle="tab" href="#exampleTabsSolidFour" aria-controls="exampleTabsSolidFour" role="tab" aria-expanded="true">Expenses</a></li>
                  </ul>
                  <div class="tab-content">
                    <div class="tab-pane" id="exampleTabsSolidOne" role="tabpanel">

                        <div class="row">
            			  <div class="form-group col-sm-6">			  
            			  <label class="control-label" for="inputBasicFirstName">Purchase Date</label>
                              <div class="input-group">
                                <span class="input-group-addon">
                                  <i class="icon wb-calendar" aria-hidden="true"></i>
                                </span>
                                <input type="text" class="form-control" name="purchasedate" value="<?php echo e(old('purchasedate')); ?>" placeholder="<?php echo e(trans('app.date_of_birth')); ?>" data-plugin="datepicker">
                              </div>
                            </div>
            			  <div class="form-group col-sm-6">			  
            			  <label class="control-label" for="inputBasicFirstName">Purchase Price</label>
                             <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <input class="form-control" placeholder="" type="text" id="purchaseprice" ng-model="purchaseprice" name="purchaseprice" ng-init="purchaseprice='<?php echo e(old('purchaseprice')); ?>'" placeholder="<?php echo e(trans('app.year')); ?>">
                              </div>
                            </div>               
                        </div>

                    </div>
                    <div class="tab-pane" id="exampleTabsSolidTwo" role="tabpanel">


                        <div class="row">
            			  <div class="form-group col-sm-6">			  
            			  <label class="control-label" for="inputBasicFirstName">Sold Date</label>
                              <div class="input-group">
                                <span class="input-group-addon">
                                  <i class="icon wb-calendar" aria-hidden="true"></i>
                                </span>
                                <input type="text" class="form-control" name="solddate" value="<?php echo e(old('solddate')); ?>" placeholder="<?php echo e(trans('app.date_of_birth')); ?>" data-plugin="datepicker">
                              </div>
                            </div>
            			  <div class="form-group col-sm-6">			  
            			  <label class="control-label" for="inputBasicFirstName">Purchase Price</label>
                             <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <input class="form-control" placeholder="" type="text" id="soldprice" ng-model="soldprice" name="soldprice" ng-init="soldprice='<?php echo e(old('soldprice')); ?>'" placeholder="<?php echo e(trans('app.year')); ?>">
                              </div>
                            </div>               
                        </div>


                    </div>
                    <div class="tab-pane active" id="exampleTabsSolidThree" role="tabpanel">


                        <div class="row">
            			  <div class="form-group col-sm-3">			  
            			  <label class="control-label" for="inputBasicFirstName">TradeIn Date</label>
                              <div class="input-group">
                                <span class="input-group-addon">
                                  <i class="icon wb-calendar" aria-hidden="true"></i>
                                </span>
                                <input type="text" class="form-control" name="tradeindate" value="<?php echo e(old('tradeindate')); ?>" placeholder="<?php echo e(trans('app.date_of_birth')); ?>" data-plugin="datepicker">
                              </div>
                            </div>
            			  <div class="form-group col-sm-3">			  
            			  <label class="control-label" for="inputBasicFirstName">TradeIn Price</label>
                             <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <input class="form-control" placeholder="" type="text" id="tradeinprice" ng-model="tradeinprice" name="tradeinprice" ng-init="tradeinprice='<?php echo e(old('tradeinprice')); ?>'" placeholder="<?php echo e(trans('app.year')); ?>">
                              </div>
                            </div>  
                            <div class="form-group col-sm-6">
                                <label class="control-label" for="inputBasicFirstName">Search By Vin</label>
                				<div class="input-group">
                                    <input type="text" class="form-control" id="tradeinvin" ng-model="tradeinvin" name="tradeinvin" ng-init="tradeinvin='<?php echo e(old('tradeinvin')); ?>'" placeholder="Search By <?php echo e(trans('app.vin')); ?>" required>
                                    <span class="input-group-btn">
                                      <button ng-click="searchVinNumber()" class="btn btn-primary"><i class="icon wb-search" aria-hidden="true"></i></button>
                                    </span>
                                  </div>                                
                            </div>
                        </div>


                    </div>
                    <div class="tab-pane" id="exampleTabsSolidFour" role="tabpanel">

                        <div class="row">
            			  <div class="form-group col-sm-3">			  
            			  <label class="control-label" for="inputBasicFirstName"> Date</label>
                              <div class="input-group">
                                <span class="input-group-addon">
                                  <i class="icon wb-calendar" aria-hidden="true"></i>
                                </span>
                                <input type="text" class="form-control" name="expensedate" value="<?php echo e(old('expensedate')); ?>" placeholder="<?php echo e(trans('app.date_of_birth')); ?>" data-plugin="datepicker">
                              </div>
                            </div>
            			  <div class="form-group col-sm-4">			  
            			  <label class="control-label" for="inputBasicFirstName"> Expense</label>
                              <input class="form-control" placeholder="" type="text" id="purchaseprice" ng-model="purchaseprice" name="purchaseprice" ng-init="purchaseprice='<?php echo e(old('purchaseprice')); ?>'" placeholder="<?php echo e(trans('app.year')); ?>">
                            </div>                            
            			  <div class="form-group col-sm-4">			  
            			  <label class="control-label" for="inputBasicFirstName">Cost</label>
                             <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <input class="form-control" placeholder="" type="text" id="purchaseprice" ng-model="purchaseprice" name="purchaseprice" ng-init="purchaseprice='<?php echo e(old('purchaseprice')); ?>'" placeholder="<?php echo e(trans('app.year')); ?>">
                              </div>
                            </div>   
                            <div class="form-group col-sm-1">
                              <label class="control-label" for="inputBasicFirstName">&nbsp;</label>
                              <button ng-click="searchVinNumber()" class="btn btn-success"><i class="icon fa-plus" aria-hidden="true"></i></button>
                              </div>                            
                        </div>



                    </div>
                  </div>
                </div>	    
		    
		</div>
			
		<div class="row">
			<div class="form-group col-sm-12">
			<label class="control-label" for="inputBasicFirstName">Notes</label>
			<textarea class="form-control" id="notes" name="notes" value="<?php echo e(old('notes')); ?>" placeholder="Enter notes" autocomplete="off"></textarea>
		 </div>
		  
		</div>
	  
	</div>
	<div class="col-sm-4">
	  <!-- Example Basic Form Without Label -->
         <div class="widget">
            <div class="widget-header white bg-cyan-600 padding-30 clearfix">
              <a class="avatar avatar-100 pull-left margin-right-20" href="javascript:void(0)">
                <img src="http://crud-itsjstme.c9users.io/global/portraits/5.jpg" alt="">
              </a>
              <div class="pull-left">
                <div class="font-size-20 margin-bottom-15"></div>
                <p class="margin-bottom-5 text-nowrap"><i class="icon wb-map margin-right-10" aria-hidden="true"></i>
                  <span class="text-break"></span>
                </p>
                <p class="margin-bottom-5 text-nowrap"><i class="icon bd-twitter margin-right-10" aria-hidden="true"></i>
                  <span class="text-break">Kolage</span>
                </p>
              </div>
            </div>
            <div class="widget-content">
              <div class="row no-space padding-vertical-20 padding-horizontal-30 text-center">
                <div class="col-xs-4">
                  <div class="counter">
                    <span class="counter-number cyan-600">102</span>
                    <div class="counter-label">Projects</div>
                  </div>
                </div>
                <div class="col-xs-4">
                  <div class="counter">
                    <span class="counter-number cyan-600">125</span>
                    <div class="counter-label">Clients</div>
                  </div>
                </div>
                <div class="col-xs-4">
                  <div class="counter">
                    <span class="counter-number cyan-600">10.8K</span>
                    <div class="counter-label">Followers</div>
                  </div>
                </div>
              </div>
            </div>
          </div>	     
			
	</div>
	<div style="clear:both"></div>
	<div class="form-group col-sm-6">
	<button type="submit" ng-disabled="userForm.$invalid" class="btn btn-primary ladda-button" data-plugin="ladda" data-style="expand-left">
			<i class="fa fa-save"></i>  Save
		<span class="ladda-spinner"></span><div class="ladda-progress" style="width: 0px;"></div>
		</button>
	
	</div>
  </div>
</form> 
</div>
<!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
</div><br/>
		

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>