<!-- Dealershipid Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('dealershipid', 'Dealershipid:'); ?>

    <?php echo Form::number('dealershipid', null, ['class' => 'form-control']); ?>

</div>

<!-- Userid Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('userid', 'Userid:'); ?>

    <?php echo Form::number('userid', null, ['class' => 'form-control']); ?>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo route('dealershipUsers.index'); ?>" class="btn btn-default">Cancel</a>
</div>
