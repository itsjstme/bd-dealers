<table class="table table-responsive" id="inventories-table">
    <thead>
        <th>Starred</th>
        <th>Styleid</th>
        <th>Tradeinid</th>
        <th>Vin</th>
        <th>Notes</th>
        <th>Mileage</th>
        <th>Offerprice</th>
        <th>Purchaseprice</th>
        <th>Soldprice</th>
        <th>Tradeinprice</th>
        <th>Purchasedate</th>
        <th>Solddate</th>
        <th>Tradeindate</th>
        <th>Deliverydate</th>
        <th>Marketdate</th>
        <th>Createdby</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $inventories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $inventory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $inventory->starred; ?></td>
            <td><?php echo $inventory->styleid; ?></td>
            <td><?php echo $inventory->tradeinid; ?></td>
            <td><?php echo $inventory->vin; ?></td>
            <td><?php echo $inventory->notes; ?></td>
            <td><?php echo $inventory->mileage; ?></td>
            <td><?php echo $inventory->offerprice; ?></td>
            <td><?php echo $inventory->purchaseprice; ?></td>
            <td><?php echo $inventory->soldprice; ?></td>
            <td><?php echo $inventory->tradeinprice; ?></td>
            <td><?php echo $inventory->purchasedate; ?></td>
            <td><?php echo $inventory->solddate; ?></td>
            <td><?php echo $inventory->tradeindate; ?></td>
            <td><?php echo $inventory->deliverydate; ?></td>
            <td><?php echo $inventory->marketdate; ?></td>
            <td><?php echo $inventory->createdBy; ?></td>
            <td>
                <?php echo Form::open(['route' => ['inventories.destroy', $inventory->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('inventories.show', [$inventory->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('inventories.edit', [$inventory->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>