<!-- Offerids Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('offerids', 'Offerids:'); ?>

    <?php echo Form::text('offerids', null, ['class' => 'form-control']); ?>

</div>

<!-- Title Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('title', 'Title:'); ?>

    <?php echo Form::text('title', null, ['class' => 'form-control']); ?>

</div>

<!-- Price Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('price', 'Price:'); ?>

    <?php echo Form::number('price', null, ['class' => 'form-control']); ?>

</div>

<!-- Expires Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('expires', 'Expires:'); ?>

    <?php echo Form::date('expires', null, ['class' => 'form-control']); ?>

</div>

<!-- Createdby Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('createdBy', 'Createdby:'); ?>

    <?php echo Form::number('createdBy', null, ['class' => 'form-control']); ?>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo route('offers.index'); ?>" class="btn btn-default">Cancel</a>
</div>
