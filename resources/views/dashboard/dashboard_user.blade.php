@extends('layouts.template')


@section('header')
   <!--
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular-touch.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular-animate.js"></script>
    <script src="http://ui-grid.info/docs/grunt-scripts/csv.js"></script>
    <script src="http://ui-grid.info/docs/grunt-scripts/pdfmake.js"></script>
    <script src="http://ui-grid.info/docs/grunt-scripts/vfs_fonts.js"></script>
    <script src="/angular-ui-grid/ui-grid.js"></script>
    <script src="/angular-ui-grid/ui-grid.css"></script>
    -->
    <script src="/css/cards.css"></script>
@endsection

@section('footer')
<!--
   <script src="/js/dashboard.js"></script>
 -->
@endsection


@section('content')
<div class="page-content padding-20 container-fluid">
<!------------------------------ Start Alert message--------------
    <div class="alert alert-primary alert-dismissible alertDismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    	<span aria-hidden="true">×</span>
      </button>
     {{ trans('app.welcome')}}  {{Auth::user()->first_name}} {{Auth::user()->last_name}} !
    </div>
<!-------------------------------- End alert message--------------->
	
<!-------------------------------- start second step graph--------------->
<!--main-->

<!-------------------------------- Start first step graph--------------->
<div class="row" data-plugin="matchHeight" data-by-row="true">
<!-- First Row -->
<div class="col-lg-3 col-sm-6 col-xs-12 info-panel">
  <div class="widget widget-shadow">
	<div class="widget-content bg-white padding-20">
	  <button type="button" class="btn btn-floating btn-sm btn-primary">
		<i class="icon wb-users" aria-hidden="true"></i>
	  </button>
	  <span class="margin-left-15 font-weight-400 example-title">Total Offers</span>
	  <div class="content-text text-center margin-bottom-0">
		<i class="text-danger icon wb-triangle-up font-size-20">
				  </i>
		<span class="font-size-40 font-weight-100">88</span>
		<p class="blue-grey-400 font-weight-100 margin-0">Total registered users</p>
	  </div>
	</div>
  </div>
</div>
<div class="col-lg-3 col-sm-6 col-xs-12 info-panel">
  <div class="widget widget-shadow">
	<div class="widget-content bg-white padding-20">
	  <button type="button" class="btn btn-floating btn-sm btn-primary">
		<i class="icon wb-user-add" aria-hidden="true"></i>
	  </button>
	  <span class="margin-left-15 font-weight-400 example-title"> New Offers</span>
	  <div class="content-text text-center margin-bottom-0">
		<i class="text-danger icon wb-triangle-up font-size-20">
				  </i>
		<span class="font-size-40 font-weight-100">16</span>
		<p class="blue-grey-400 font-weight-100 margin-0">New users this month</p>
	  </div>
	</div>
  </div>
</div>
<div class="col-lg-3 col-sm-6 col-xs-12 info-panel">
  <div class="widget widget-shadow">
	<div class="widget-content bg-white padding-20">
	  <button type="button" class="btn btn-floating btn-sm btn-primary">
		<i class="icon wb-eye"></i>
	  </button>
	  <span class="margin-left-15 font-weight-400 example-title">Visitors</span>
	  <div class="content-text text-center margin-bottom-0">
		<i class="text-danger icon wb-triangle-up font-size-20">
				  </i>
		<span class="font-size-40 font-weight-100">269</span>
		<p class="blue-grey-400 font-weight-100 margin-0">This month visitors</p>
	  </div>
	</div>
  </div>
</div>
<div class="col-lg-3 col-sm-6 col-xs-12 info-panel">
  <div class="widget widget-shadow">
	<div class="widget-content bg-white padding-20">
	  <button type="button" class="btn btn-floating btn-sm btn-primary">
		<i class="icon wb-calendar" aria-hidden="true"></i>
	  </button>
	  <span class="margin-left-15 font-weight-400 example-title">Visitors</span>
	  <div class="content-text text-center margin-bottom-0">
		<i class="text-danger icon wb-triangle-up font-size-20">
				  </i>
		<span class="font-size-40 font-weight-100">1</span>
		<p class="blue-grey-400 font-weight-100 margin-0">Today visitors</p>
	  </div>
	</div>
  </div>
</div>
<!-- End First Row -->
</div>
<!-------------------------------- end first step graph--------------->

<div class="row" data-plugin="matchHeight" data-by-row="true">
      
      
      <div class="col-sm-4">
      
      	<div class="panel panel-default">
          <div class="panel-body">
            <p class="lead">Urbanization</p>
            <p>45 Followers, 13 Posts</p>
            <p>
              <img src="https://lh3.googleusercontent.com/uFp_tsTJboUY7kue5XAsGA=s28" height="28px" width="28px">
            </p>
          </div>
        </div>

        
      </div><!--/col-->
      
      <div class="col-sm-4">
      
      	<div class="panel panel-default">
          <div class="panel-body">
            <p class="lead">Urbanization</p>
            <p>45 Followers, 13 Posts</p>
            <p>
              <img src="https://lh3.googleusercontent.com/uFp_tsTJboUY7kue5XAsGA=s28" height="28px" width="28px">
            </p>
          </div>
        </div>

        
      </div><!--/col-->
      
      <div class="col-sm-4">
      
      	<div class="panel panel-default">
          <div class="panel-body">
            <p class="lead">Urbanization</p>
            <p>45 Followers, 13 Posts</p>
            <p>
              <img src="https://lh3.googleusercontent.com/uFp_tsTJboUY7kue5XAsGA=s28" height="28px" width="28px">
            </p>
          </div>
        </div>

        
      </div><!--/col-->
   
    
      <div class="col-sm-4">
      
      	<div class="panel panel-default">
          <div class="panel-body">
            <p class="lead">Urbanization</p>
            <p>45 Followers, 13 Posts</p>
            <p>
              <img src="https://lh3.googleusercontent.com/uFp_tsTJboUY7kue5XAsGA=s28" height="28px" width="28px">
            </p>
          </div>
        </div>

        
      </div><!--/col-->
      
      <div class="col-sm-4">
      
      	<div class="panel panel-default">
          <div class="panel-body">
            <p class="lead">Urbanization</p>
            <p>45 Followers, 13 Posts</p>
            <p>
              <img src="https://lh3.googleusercontent.com/uFp_tsTJboUY7kue5XAsGA=s28" height="28px" width="28px">
            </p>
          </div>
        </div>

        
      </div><!--/col-->
      
      <div class="col-sm-4">
      
      	<div class="panel panel-default">
          <div class="panel-body">
            <p class="lead">Urbanization</p>
            <p>45 Followers, 13 Posts</p>
            <p>
              <img src="https://lh3.googleusercontent.com/uFp_tsTJboUY7kue5XAsGA=s28" height="28px" width="28px">
            </p>
          </div>
        </div>

        
      </div><!--/col-->
      <div class="col-sm-4">
      
      	<div class="panel panel-default">
          <div class="panel-body">
            <p class="lead">Urbanization</p>
            <p>45 Followers, 13 Posts</p>
            <p>
              <img src="https://lh3.googleusercontent.com/uFp_tsTJboUY7kue5XAsGA=s28" height="28px" width="28px">
            </p>
          </div>
        </div>

        
      </div><!--/col-->
      <div class="col-sm-4">
      
      	<div class="panel panel-default">
          <div class="panel-body">
            <p class="lead">Urbanization</p>
            <p>45 Followers, 13 Posts</p>
            <p>
              <img src="https://lh3.googleusercontent.com/uFp_tsTJboUY7kue5XAsGA=s28" height="28px" width="28px">
            </p>
          </div>
        </div>

        
      </div><!--/col-->      
    
    <br>

      

  </div><!--/main row-->

 <!-------------------------------- end second step graph---------------> 		
</div>

@stop
