
@extends('layouts.template')

@section('header')
<link rel="stylesheet" href="{{URL::to('/')}}/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
<link rel="stylesheet" href="{{URL::to('/')}}/global/vendor/bootstrap-maxlength/bootstrap-maxlength.css">
<link rel="stylesheet" href="{{URL::to('/')}}/global/vendor/jt-timepicker/jquery-timepicker.css">
<link rel="stylesheet" href="{{URL::to('/')}}/assets/examples/css/forms/advanced.css">
<link rel="stylesheet" href="{{URL::to('/')}}/angular-ui-grid/ui-grid.min.css">
    
@endsection

@section('footer')

<script src="/js/angular.min.js" type="text/javascript"></script>
<script src="/js/angular-bootstrap/ui-bootstrap.min.js"></script>
<script src="/js/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>
<script src="/js/angular-smart-table/dist/smart-table.js"></script>
<script src="/js/offer.js" type="text/javascript"></script>
@endsection


@section('content') 



<style>
p.redcolor{color:red; font-size:16px;}
.spancolor{color:red;}
.help-block{color:red;}
</style>

<div class="page-header">
  <h1 class="page-title font_lato">Create a New Offer</h1>
  <div class="page-header-actions">
  <ol class="breadcrumb">
		<li><a href="{{URL::to('/dashboard')}}">{{ trans('app.home')}}</a></li>
		<li><a href="{{URL::to('offers')}}">{{ trans('app.offers')}}</a></li>
		<li class="active">{{ trans('app.create')}}</li>
	</ol>
  </div>
</div>
	
<div class="page-content" ng-app="app" ng-cloak>	
<div class="panel">
<div class="panel-body container-fluid">
<!------------------------start insert, update, delete message  ---------------->
<div class="row">
@if(session('msg_success'))
	<div class="alert dark alert-icon alert-success alert-dismissible alertDismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">×</span>
	  </button>
	  <i class="icon wb-check" aria-hidden="true"></i> 
	  {{session('msg_success')}}
	</div>
@endif
@if(session('msg_update'))
	<div class="alert dark alert-icon alert-info alert-dismissible alertDismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">×</span>
	  </button>
	  <i class="icon wb-check" aria-hidden="true"></i> 
	  {{session('msg_update')}}
	</div>
@endif
@if(session('msg_delete'))
	<div class="alert dark alert-icon alert-danger alert-dismissible alertDismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">×</span>
	  </button>
	  <i class="icon wb-check" aria-hidden="true"></i> 
	  {{session('msg_delete')}}
	</div>
@endif
</div>
<form  name="userForm" action="{{URL::to('store')}}" ng-submit="submitForm(userForm.$valid)" novalidate  id="demo-form2" data-parsley-validate="" method="post" novalidate="">
		{{ csrf_field() }}
<div class="row row-lg">
	<div class="col-sm-8" style="border-right: 1px dotted #ddd;">
	  <!-- Example Basic Form -->	              
			<div class="row">
				
			<div class="col-sm-12">
				<input type="hidden"  id="title" ng-model="offerids" name="offerids" ng-init="offerids='{{ old('offerids') }}'" required>
			</div>
			  <div class="form-group col-sm-4">
				<label class="control-label" for="inputBasicFirstName">Title<span class="spancolor">*</span> </label>
				<input type="text" class="form-control" id="title" ng-model="title" name="title" ng-init="title='{{ old('title') }}'" placeholder="Enter title" required>
			  </div>
			  <div class="form-group col-sm-3">
				<label class="control-label" for="inputBasicFirstName">Expires</label>
				<select ng-model="expires"  class="form-control" name="expires" required ng-init="expires = '{{ old('expires') }}'">
					<option value="">Select days</option>	
					<option value="10">10 days</option>
					<option value="13">13 days</option>
					<option value="15">15 days</option>
					<option value="19">19 days</option>
					<option value="22">22 days</option>
					<option value="30">30 days</option>					
				</select>				
			  </div>			  
			  <div class="form-group col-sm-2">
				<label class="control-label" for="inputBasicLastName">Price<span class="spancolor">*</span> </label>
				<input type="text" class="form-control" id="offerprice" name="offerprice" ng-model="offerprice" ng-init="offerprice='{{ old('offerprice') }}'" placeholder="Enter Price" required data-plugin="formatter"
                    data-pattern="$[[999]],[[999]],[[999]].[[99]]" />
                    <p class="help-block">$999,999,999.99</p>
			  </div>			  
			  <div class="form-group col-sm-2">
				<label class="control-label" for="inputBasicLastName">Lowest Price<span class="spancolor">*</span> </label>
				<input type="text" class="form-control" id="lowestprice" name="lowestprice" ng-model="lowestprice" ng-init="lowestprice='{{ old('lowestprice') }}'" placeholder="Enter Price" required data-plugin="formatter"
                    data-pattern="$[[999]],[[999]],[[999]].[[99]]" />
                    <p class="help-block">$999,999,999.99</p>
			  </div>
			  <div class="form-group col-sm-12">
			  	<p>Only Vehicles not alreadybeing offered will show up.</p>
				<label class="control-label" for="inputBasicFirstName">Search Inventory<span class="spancolor">*</span> </label>
						<section id="section-pipe" class="clearfix" ng-controller="pipeCtrl">
							<div class="table-container" ng-controller="pipeCtrl as mc">
								<table class="table" st-pipe="mc.callServer" st-table="mc.displayed">
									<thead>
									<tr>
										<th class st-sort="id">id</th>
										<th st-sort-default="reverse" st-sort="name">Description</th>
										<th st-sort="age">Mileage</th>
										<th st-sort="saved">Price</th>
									</tr>
									<tr>
										<th>&nbsp;</th>
										<th><input class="form-control" st-search="name"/></th>
										<th><input class="form-control" st-search="age"/></th>
										<th><input class="form-control" st-search="saved"/></th>
									</tr>
									</thead>
									<tbody ng-show="!mc.isLoading">
									<tr ng-repeat="row in mc.displayed">
										<td class="col-sm-1"><input type="checkbox" value="@{{row.id}}" ng-click="myFunc()"/></td>
										<td class="col-sm-3">@{{row.name}}</td>
										<td class="col-sm-3">@{{row.age}}</td>
										<td class="col-sm-3">@{{row.saved}}</td>
									</tr>
									</tbody>
									<tbody ng-show="mc.isLoading">
									<tr>
										<td colspan="4" class="text-center"><div class="loading-indicator"></div>
										</td>
									</tr>
									</tbody>
									<tfoot>
									<tr>
										<td class="text-center" st-pagination="" st-items-by-page="10" colspan="4">
										</td>
									</tr>
									</tfoot>
								</table>
							</div>
						</section>
			  </div>		  
			</div>
			<div class="row">
					<div class="form-group col-sm-6">
						<button type="submit"  class="btn btn-primary ladda-button">
						<i class="fa fa-save"></i>  Save Offer
						</button>
					</div>	
			</div>
	</div>
	
	<div class="row">
			<div class="col-sm-4">
		 <div class="widget widget-shadow">
            <div class="widget-header bg-blue-600 white padding-15 clearfix">
              <a class="avatar avatar-lg pull-left margin-right-20" href="javascript:void(0)">
                <img src="http://crud-itsjstme.c9users.io/global/vehicle-icon-12441.png" alt="">
              </a>
              <div class="font-size-18" ng-bind="title"></div>
              <div class="grey-300 font-size-14" ng-bind="price"></div>
              <div class="grey-300 font-size-14">expires in <span ng-bind="expires"></span> days</div>
            </div>
            <div class="widget-content">
              <ul class="list-group list-group-bordered">
                <li class="list-group-item" ng-bind="offer"></li>
              </ul>
            </div>
          </div>
					
			</div>
	</div>
  </div>
</form> 
</div>
<!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
</div><br/>





@stop