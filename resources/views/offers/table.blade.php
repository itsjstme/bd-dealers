<table class="table table-responsive" id="offers-table">
    <thead>
        <th>Offerids</th>
        <th>Title</th>
        <th>Price</th>
        <th>Expires</th>
        <th>Createdby</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($offers as $offers)
        <tr>
            <td>{!! $offers->offerids !!}</td>
            <td>{!! $offers->title !!}</td>
            <td>{!! $offers->price !!}</td>
            <td>{!! $offers->expires !!}</td>
            <td>{!! $offers->createdBy !!}</td>
            <td>
                {!! Form::open(['route' => ['offers.destroy', $offers->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('offers.show', [$offers->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('offers.edit', [$offers->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>