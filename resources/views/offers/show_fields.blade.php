<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $offers->id !!}</p>
</div>

<!-- Offerids Field -->
<div class="form-group">
    {!! Form::label('offerids', 'Offerids:') !!}
    <p>{!! $offers->offerids !!}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{!! $offers->title !!}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Price:') !!}
    <p>{!! $offers->price !!}</p>
</div>

<!-- Expires Field -->
<div class="form-group">
    {!! Form::label('expires', 'Expires:') !!}
    <p>{!! $offers->expires !!}</p>
</div>

<!-- Createdby Field -->
<div class="form-group">
    {!! Form::label('createdBy', 'Createdby:') !!}
    <p>{!! $offers->createdBy !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $offers->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $offers->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $offers->deleted_at !!}</p>
</div>

