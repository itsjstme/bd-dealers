<!-- Styleid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('styleid', 'Styleid:') !!}
    {!! Form::number('styleid', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Year Field -->
<div class="form-group col-sm-6">
    {!! Form::label('year', 'Year:') !!}
    {!! Form::number('year', null, ['class' => 'form-control']) !!}
</div>

<!-- Model Field -->
<div class="form-group col-sm-6">
    {!! Form::label('model', 'Model:') !!}
    {!! Form::text('model', null, ['class' => 'form-control']) !!}
</div>

<!-- Trime Field -->
<div class="form-group col-sm-6">
    {!! Form::label('trime', 'Trime:') !!}
    {!! Form::text('trime', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('edmundMakeModels.index') !!}" class="btn btn-default">Cancel</a>
</div>
