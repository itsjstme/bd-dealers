<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $edmundMakeModel->id !!}</p>
</div>

<!-- Styleid Field -->
<div class="form-group">
    {!! Form::label('styleid', 'Styleid:') !!}
    <p>{!! $edmundMakeModel->styleid !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $edmundMakeModel->name !!}</p>
</div>

<!-- Year Field -->
<div class="form-group">
    {!! Form::label('year', 'Year:') !!}
    <p>{!! $edmundMakeModel->year !!}</p>
</div>

<!-- Model Field -->
<div class="form-group">
    {!! Form::label('model', 'Model:') !!}
    <p>{!! $edmundMakeModel->model !!}</p>
</div>

<!-- Trime Field -->
<div class="form-group">
    {!! Form::label('trime', 'Trime:') !!}
    <p>{!! $edmundMakeModel->trime !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $edmundMakeModel->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $edmundMakeModel->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $edmundMakeModel->deleted_at !!}</p>
</div>

