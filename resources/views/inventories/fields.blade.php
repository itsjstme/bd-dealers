<!-- Starred Field -->
<div class="form-group col-sm-6">
    {!! Form::label('starred', 'Starred:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('starred', false) !!}
        {!! Form::checkbox('starred', '1', null) !!} 1
    </label>
</div>

<!-- Styleid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('styleid', 'Styleid:') !!}
    {!! Form::number('styleid', null, ['class' => 'form-control']) !!}
</div>

<!-- Tradeinid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tradeinid', 'Tradeinid:') !!}
    {!! Form::number('tradeinid', null, ['class' => 'form-control']) !!}
</div>

<!-- Vin Field -->
<div class="form-group col-sm-6">
    {!! Form::label('vin', 'Vin:') !!}
    {!! Form::text('vin', null, ['class' => 'form-control']) !!}
</div>

<!-- Notes Field -->
<div class="form-group col-sm-6">
    {!! Form::label('notes', 'Notes:') !!}
    {!! Form::text('notes', null, ['class' => 'form-control']) !!}
</div>

<!-- Mileage Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mileage', 'Mileage:') !!}
    {!! Form::number('mileage', null, ['class' => 'form-control']) !!}
</div>

<!-- Offerprice Field -->
<div class="form-group col-sm-6">
    {!! Form::label('offerprice', 'Offerprice:') !!}
    {!! Form::number('offerprice', null, ['class' => 'form-control']) !!}
</div>

<!-- Purchaseprice Field -->
<div class="form-group col-sm-6">
    {!! Form::label('purchaseprice', 'Purchaseprice:') !!}
    {!! Form::number('purchaseprice', null, ['class' => 'form-control']) !!}
</div>

<!-- Soldprice Field -->
<div class="form-group col-sm-6">
    {!! Form::label('soldprice', 'Soldprice:') !!}
    {!! Form::number('soldprice', null, ['class' => 'form-control']) !!}
</div>

<!-- Tradeinprice Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tradeinprice', 'Tradeinprice:') !!}
    {!! Form::number('tradeinprice', null, ['class' => 'form-control']) !!}
</div>

<!-- Purchasedate Field -->
<div class="form-group col-sm-6">
    {!! Form::label('purchasedate', 'Purchasedate:') !!}
    {!! Form::date('purchasedate', null, ['class' => 'form-control']) !!}
</div>

<!-- Solddate Field -->
<div class="form-group col-sm-6">
    {!! Form::label('solddate', 'Solddate:') !!}
    {!! Form::date('solddate', null, ['class' => 'form-control']) !!}
</div>

<!-- Tradeindate Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tradeindate', 'Tradeindate:') !!}
    {!! Form::date('tradeindate', null, ['class' => 'form-control']) !!}
</div>

<!-- Deliverydate Field -->
<div class="form-group col-sm-6">
    {!! Form::label('deliverydate', 'Deliverydate:') !!}
    {!! Form::date('deliverydate', null, ['class' => 'form-control']) !!}
</div>

<!-- Marketdate Field -->
<div class="form-group col-sm-6">
    {!! Form::label('marketdate', 'Marketdate:') !!}
    {!! Form::date('marketdate', null, ['class' => 'form-control']) !!}
</div>

<!-- Createdby Field -->
<div class="form-group col-sm-6">
    {!! Form::label('createdBy', 'Createdby:') !!}
    {!! Form::number('createdBy', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('inventories.index') !!}" class="btn btn-default">Cancel</a>
</div>
