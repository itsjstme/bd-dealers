<table class="table table-responsive" id="inventories-table">
    <thead>
        <th>Starred</th>
        <th>Styleid</th>
        <th>Tradeinid</th>
        <th>Vin</th>
        <th>Notes</th>
        <th>Mileage</th>
        <th>Offerprice</th>
        <th>Purchaseprice</th>
        <th>Soldprice</th>
        <th>Tradeinprice</th>
        <th>Purchasedate</th>
        <th>Solddate</th>
        <th>Tradeindate</th>
        <th>Deliverydate</th>
        <th>Marketdate</th>
        <th>Createdby</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($inventories as $inventory)
        <tr>
            <td>{!! $inventory->starred !!}</td>
            <td>{!! $inventory->styleid !!}</td>
            <td>{!! $inventory->tradeinid !!}</td>
            <td>{!! $inventory->vin !!}</td>
            <td>{!! $inventory->notes !!}</td>
            <td>{!! $inventory->mileage !!}</td>
            <td>{!! $inventory->offerprice !!}</td>
            <td>{!! $inventory->purchaseprice !!}</td>
            <td>{!! $inventory->soldprice !!}</td>
            <td>{!! $inventory->tradeinprice !!}</td>
            <td>{!! $inventory->purchasedate !!}</td>
            <td>{!! $inventory->solddate !!}</td>
            <td>{!! $inventory->tradeindate !!}</td>
            <td>{!! $inventory->deliverydate !!}</td>
            <td>{!! $inventory->marketdate !!}</td>
            <td>{!! $inventory->createdBy !!}</td>
            <td>
                {!! Form::open(['route' => ['inventories.destroy', $inventory->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('inventories.show', [$inventory->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('inventories.edit', [$inventory->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>