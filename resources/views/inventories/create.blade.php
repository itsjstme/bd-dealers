@extends('layouts.template')


@section('header')
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular-touch.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular-animate.js"></script>
    <script src="http://ui-grid.info/docs/grunt-scripts/csv.js"></script>
    <script src="http://ui-grid.info/docs/grunt-scripts/pdfmake.js"></script>
    <script src="http://ui-grid.info/docs/grunt-scripts/vfs_fonts.js"></script>
    <script src="/angular-ui-grid/ui-grid.js"></script>
    <script src="/angular-ui-grid/ui-grid.css"></script>
    
@endsection

@section('footer')
   <script src="/js/inventory.js"></script>
@endsection


@section('content') 
<link rel="stylesheet" href="{{URL::to('/')}}/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
<link rel="stylesheet" href="{{URL::to('/')}}/global/vendor/bootstrap-maxlength/bootstrap-maxlength.css">
<link rel="stylesheet" href="{{URL::to('/')}}/global/vendor/jt-timepicker/jquery-timepicker.css">
<link rel="stylesheet" href="{{URL::to('/')}}/assets/examples/css/forms/advanced.css">

<style>
p.redcolor{color:red; font-size:16px;}
.spancolor{color:red;}
.help-block{color:red;}
</style>

<div class="page-header">
  <h1 class="page-title font_lato">{{ trans('app.create_new_car')}}</h1>
  <div class="page-header-actions">
  <ol class="breadcrumb">
		<li><a href="{{URL::to('/dashboard')}}">{{ trans('app.home')}}</a></li>
		<li><a href="{{URL::to('inventory')}}">{{ trans('app.cars')}}</a></li>
		<li class="active">{{ trans('app.create')}}</li>
	</ol>
  </div>
</div>
	
<div class="page-content">	
<div class="panel">
<div class="panel-body container-fluid">
<!------------------------start insert, update, delete message  ---------------->
<div class="row">
@if(session('msg_success'))
	<div class="alert dark alert-icon alert-success alert-dismissible alertDismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">×</span>
	  </button>
	  <i class="icon wb-check" aria-hidden="true"></i> 
	  {{session('msg_success')}}
	</div>
@endif
@if(session('msg_update'))
	<div class="alert dark alert-icon alert-info alert-dismissible alertDismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">×</span>
	  </button>
	  <i class="icon wb-check" aria-hidden="true"></i> 
	  {{session('msg_update')}}
	</div>
@endif
@if(session('msg_delete'))
	<div class="alert dark alert-icon alert-danger alert-dismissible alertDismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">×</span>
	  </button>
	  <i class="icon wb-check" aria-hidden="true"></i> 
	  {{session('msg_delete')}}
	</div>
@endif
</div>
<div ng-app="app">
<div>
<form name="inventoryForm" ng-controller="MainCtrl" novalidate="novalidate" autocomplete="off">
		{{ csrf_field() }}
		<input type="hidden" ng-model="inventory.styleid" required>
<div class="row row-lg">
	<div class="col-sm-8" style="border-right: 1px dotted #ddd;">
	  <!-- Example Basic Form -->	              
		 <div class="row bg-red-200" style="padding:5px;">
			  <div class="form-group col-sm-8">
				<label class="control-label" for="inputBasicFirstName">{{ trans('app.vin')}}<span class="spancolor">*</span> </label>
				<div class="input-group">
                    <input type="text" class="form-control" id="vin" ng-model="inventory.vin" ng-pattern="patternFirstName" name="vin" ng-pattern="/^[A-HJ-NPR-Z\\d]{8}[\\dX][A-HJ-NPR-Z\\d]{2}\\d{6}$/" placeholder="Search By {{ trans('app.vin')}}">
                    <span class="input-group-btn">
                      <button ng-click="vinNumberSearch(vinNumber)" class="btn btn-primary"><i class="icon wb-search" aria-hidden="true"></i></button>
                    </span>
                  </div>			
                  <div ng-messages="inventoryForm.vinNumber.$error">
                    <div ng-message="pattern">Must be a valid Vin Number</div>
                  </div>                  
			  </div>			
			  <div class="form-group col-sm-4">
				    <label class="control-label" for="inputBasicLastName">{{ trans('app.mileage')}}<span class="spancolor">*</span> </label>
				    <input type="text" class="form-control" id="mileage" name="inventory.mileage" ng-model="inventory.mileage" ng-init="mileage='{{ old('mileage') }}'" placeholder="{{ trans('app.mileage')}}" required>			  	
			  </div>
		</div>
		<div class="row bg-red-200" style="padding:5px;">
			  <div class="form-group col-sm-3">
				<label class="control-label" for="inputBasicFirstName">{{ trans('app.year')}}<span class="spancolor">*</span> </label>
        <select class="form-control" ng-options="year.value as year.label for year in years" name="year" ng-model="year" ng-change="changeYear(year)" ng-required="required">
          <option value selected disabled>Select Year</option>
        </select>				  
			  </div>			
			  <div class="form-group col-sm-3">
				<label class="control-label" for="inputBasicFirstName">{{ trans('app.make')}}<span class="spancolor">*</span> </label>
        <select class="form-control" id="make" ng-model="make" ng-disabled="!year"  ng-options="make.value as make.label for make in makes" ng-change="changeMake(make)" ng-required="required">
            <option value="">Select a Make</option>
        </select>				
			  </div>
			  <div class="form-group col-sm-3">
				<label class="control-label" for="inputBasicLastName">{{ trans('app.model')}}<span class="spancolor">*</span> </label>
        <select class="form-control" id="model" ng-model="model" ng-disabled="!make"  ng-options="model.value as model.label for model in models" ng-change="changeModel(model)" ng-required="required">
            <option value="">Select a Model</option>
        </select>					
			  </div>
			  <div class="form-group col-sm-3">
				<label class="control-label" for="inputBasicLastName">Trim<span class="spancolor">*</span> </label>
        <select class="form-control" id="trim" ng-model="trim" ng-disabled="!model"  ng-options="trim.value as trim.label for trim in trims" ng-required="required">
            <option value="">Select a Trim</option>
        </select>					
			  </div>			  
		</div>
		<div class="row">
		            <hr>
                <div class="nav-tabs-horizontal">
                  <ul class="nav nav-tabs nav-tabs-solid" data-plugin="nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a data-toggle="tab" href="#exampleTabsSolidOne" aria-controls="exampleTabsSolidOne" role="tab" aria-expanded="true">Purchase and Sales Information</a></li>
                    <li role="presentation" class=""><a data-toggle="tab" href="#exampleTabsSolidThree" aria-controls="exampleTabsSolidThree" role="tab" aria-expanded="false">TradeIn Information</a></li>
                    <li role="presentation" class=""><a data-toggle="tab" href="#exampleTabsSolidFour" aria-controls="exampleTabsSolidFour" role="tab" aria-expanded="false">Expenses</a></li>
                  </ul>
                  <div class="tab-content">
                    <div class="tab-pane active" id="exampleTabsSolidOne" role="tabpanel">
                        <div class="row">
                          <div class="col-sm-6">
                            <label class="control-label" for="inputBasicFirstName">Purchase</label>
                          </div>
                          <div class="col-sm-6">
                            <label class="control-label" for="inputBasicFirstName">Sales</label>
                          </div>                          
            			        <div class="form-group col-sm-3">			 
                              <div class="input-group">
                                <span class="input-group-addon">
                                  <i class="icon wb-calendar" aria-hidden="true"></i>
                                </span>
                                <input type="text" class="form-control" name="purchasedate" ng-model="inventory.purchasedate" ng-init="inventory.purchasedate='{{ old('inventory.purchasedate') }}'" placeholder="Date" data-plugin="datepicker" ng-required="required" required>
                              </div>
                            </div>
            			          <div class="form-group col-sm-3">			  
            			           <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <input class="form-control" ng-required="required" type="text" id="purchaseprice" ng-model="inventory.purchaseprice" name="purchaseprice" ng-init="purchaseprice='{{ old('purchaseprice') }}'" placeholder="Price" required>
                              </div>
                            </div>               
            			         <div class="form-group col-sm-3">			  
            			            <div class="input-group">
                                <span class="input-group-addon">
                                  <i class="icon wb-calendar" aria-hidden="true"></i>
                                </span>
                                <input type="text" class="form-control" name="solddate" ng-model="inventory.solddate" value="{{ old('solddate') }}" placeholder="Date" data-plugin="datepicker">
                              </div>
                            </div>
            			          <div class="form-group col-sm-3">			  
            			           <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <input class="form-control" type="text" id="soldprice" ng-model="inventory.soldprice" name="soldprice" ng-init="soldprice='{{ old('soldprice') }}'" placeholder="Price">
                              </div>
                            </div>              
                        </div>
                    </div>
                    <div class="tab-pane" id="exampleTabsSolidThree" role="tabpanel">
                        <div class="row">
            			  <div class="form-group col-sm-3">			  
            			  <label class="control-label" for="inputBasicFirstName">TradeIn Date</label>
                              <div class="input-group">
                                <span class="input-group-addon">
                                  <i class="icon wb-calendar" aria-hidden="true"></i>
                                </span>
                                <input type="text" class="form-control" name="tradeindate" ng-model="inventory.tradeindate" value="{{ old('tradeindate') }}" placeholder="TradeIn Date" data-plugin="datepicker">
                              </div>
                            </div>
            			  <div class="form-group col-sm-3">			  
            			  <label class="control-label" for="inputBasicFirstName">TradeIn Price</label>
                             <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <input class="form-control" placeholder="" type="text" id="tradeinprice" ng-model="inventory.tradeinprice" name="tradeinprice" ng-init="tradeinprice='{{ old('tradeinprice') }}'" placeholder="TradeIn Price">
                              </div>
                            </div>  
                            <div class="form-group col-sm-5">
                                <label class="control-label" for="inputBasicFirstName">Search By Vin or Year Make Model</label>
                				<div class="input-group">
                                    <input type="text" class="form-control" id="tradeinvin" ng-model="tradeinvin" name="tradeinvin" ng-init="tradeinvin='{{ old('tradeinvin') }}'" placeholder="Search By Vin or Year Make Model" >
                                    <span class="input-group-btn">
                                      <button ng-click="searchInventory()" class="btn btn-primary"><i class="icon wb-search" aria-hidden="true"></i></button>
                                    </span>
                                  </div>                                
                            </div>
                            <div class="form-group col-sm-1">
                              <label class="control-label" for="inputBasicFirstName">&nbsp;</label>
                              <button ng-disabled="userForm.$invalid" ng-click="searchVinNumber()" class="btn btn-success"><i class="icon fa-plus" aria-hidden="true"></i></button>
                              </div>                             
                        </div>


                    </div>
                    <div class="tab-pane" id="exampleTabsSolidFour" role="tabpanel">

                        <div class="row">
            			  <div class="form-group col-sm-3">			  
            			  <label class="control-label" for="inputBasicFirstName"> Date</label>
                              <div class="input-group">
                                <span class="input-group-addon">
                                  <i class="icon wb-calendar" aria-hidden="true"></i>
                                </span>
                                <input type="text" class="form-control" name="expensedate" value="{{ old('expensedate') }}" placeholder="Expense Date" data-plugin="datepicker">
                              </div>
                            </div>
            			  <div class="form-group col-sm-4">			  
            			  <label class="control-label" for="inputBasicFirstName"> Expense</label>
                              <input class="form-control" type="text" id="expense" ng-model="expense" name="expense" ng-init="expense='{{ old('expense') }}'" placeholder="Expense">
                            </div>                            
            			  <div class="form-group col-sm-4">			  
            			  <label class="control-label" for="inputBasicFirstName">Cost</label>
                             <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <input class="form-control" type="text" id="expensecost" ng-model="expensecost" name="expensecost" ng-init="expensecost='{{ old('expensecost') }}'" placeholder="Cost">
                              </div>
                            </div>   
                            <div class="form-group col-sm-1">
                              <label class="control-label" for="inputBasicFirstName">&nbsp;</label>
                              <button ng-disabled="userForm.$invalid" ng-click="searchVinNumber()" class="btn btn-success"><i class="icon fa-plus" aria-hidden="true"></i></button>
                              </div>                            
                        </div>



                    </div>
                  </div>
                </div>	    
		    
		</div>
			
		<div class="row">
			<div class="form-group col-sm-12">
			<label class="control-label" for="inputBasicFirstName">Notes</label>
			<textarea class="form-control" id="notes" ng-model="inventory.notes" name="notes" value="{{ old('notes') }}" placeholder="Enter notes" autocomplete="off"></textarea>
		 </div>
		  
		</div>
	  
	</div>
	<div class="col-sm-4">
	  <!-- Example Basic Form Without Label -->
	  <div class="widget widget-shadow">
            <div class="widget-content">
              <ul class="list-group list-group-bordered">
                <li class="list-group-item">
                  <ul class="list-unstyled list-inline">
                    <li>
                      <a class="avatar avatar-lg margin-5" href="javascript:void(0)">
                        <img src="http://crud-itsjstme.c9users.io/global/portraits/7.jpg" alt="">
                      </a>
                    </li>
                    <li>
                      <a class="avatar avatar-lg margin-5" href="javascript:void(0)">
                        <img src="http://crud-itsjstme.c9users.io/global/portraits/8.jpg" alt="">
                      </a>
                    </li>
                    <li>
                      <a class="avatar avatar-lg margin-5" href="javascript:void(0)">
                        <img src="http://crud-itsjstme.c9users.io/global/portraits/9.jpg" alt="">
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="list-group-item">
                  <i class="icon wb-inbox" aria-hidden="true" draggable="true"></i><span ng-bind="carName"></span>
                </li>
                <li class="list-group-item">
                  <i class="icon wb-user" aria-hidden="true" draggable="true"></i>$<span ng-bind="purchaseprice"></span>
                </li>                
                <li class="list-group-item">
                  <i class="icon wb-user" aria-hidden="true" draggable="true"></i><span ng-bind="mileage"></span>&nbsp;miles
                </li>
              </ul>
            </div>
          </div>
	 
			
	</div>
	<div style="clear:both"></div>
	<div class="form-group col-sm-6">
	<button type="submit" ng-click="addInventory(inventory)" ng-disabled="inventoryForm.$invalid" class="btn btn-primary ladda-button" data-plugin="ladda" data-style="expand-left">
			<i class="fa fa-save"></i>  Save
		<span class="ladda-spinner"></span><div class="ladda-progress" style="width: 0px;"></div>
		</button>
	

	<span ng-bind="master | json"></span>
	
	</div>
  </div>
</form> 
</div>
</div>
</div>
<!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
</div><br/>


		

@stop