<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $inventory->id !!}</p>
</div>

<!-- Starred Field -->
<div class="form-group">
    {!! Form::label('starred', 'Starred:') !!}
    <p>{!! $inventory->starred !!}</p>
</div>

<!-- Styleid Field -->
<div class="form-group">
    {!! Form::label('styleid', 'Styleid:') !!}
    <p>{!! $inventory->styleid !!}</p>
</div>

<!-- Tradeinid Field -->
<div class="form-group">
    {!! Form::label('tradeinid', 'Tradeinid:') !!}
    <p>{!! $inventory->tradeinid !!}</p>
</div>

<!-- Vin Field -->
<div class="form-group">
    {!! Form::label('vin', 'Vin:') !!}
    <p>{!! $inventory->vin !!}</p>
</div>

<!-- Notes Field -->
<div class="form-group">
    {!! Form::label('notes', 'Notes:') !!}
    <p>{!! $inventory->notes !!}</p>
</div>

<!-- Mileage Field -->
<div class="form-group">
    {!! Form::label('mileage', 'Mileage:') !!}
    <p>{!! $inventory->mileage !!}</p>
</div>

<!-- Offerprice Field -->
<div class="form-group">
    {!! Form::label('offerprice', 'Offerprice:') !!}
    <p>{!! $inventory->offerprice !!}</p>
</div>

<!-- Purchaseprice Field -->
<div class="form-group">
    {!! Form::label('purchaseprice', 'Purchaseprice:') !!}
    <p>{!! $inventory->purchaseprice !!}</p>
</div>

<!-- Soldprice Field -->
<div class="form-group">
    {!! Form::label('soldprice', 'Soldprice:') !!}
    <p>{!! $inventory->soldprice !!}</p>
</div>

<!-- Tradeinprice Field -->
<div class="form-group">
    {!! Form::label('tradeinprice', 'Tradeinprice:') !!}
    <p>{!! $inventory->tradeinprice !!}</p>
</div>

<!-- Purchasedate Field -->
<div class="form-group">
    {!! Form::label('purchasedate', 'Purchasedate:') !!}
    <p>{!! $inventory->purchasedate !!}</p>
</div>

<!-- Solddate Field -->
<div class="form-group">
    {!! Form::label('solddate', 'Solddate:') !!}
    <p>{!! $inventory->solddate !!}</p>
</div>

<!-- Tradeindate Field -->
<div class="form-group">
    {!! Form::label('tradeindate', 'Tradeindate:') !!}
    <p>{!! $inventory->tradeindate !!}</p>
</div>

<!-- Deliverydate Field -->
<div class="form-group">
    {!! Form::label('deliverydate', 'Deliverydate:') !!}
    <p>{!! $inventory->deliverydate !!}</p>
</div>

<!-- Marketdate Field -->
<div class="form-group">
    {!! Form::label('marketdate', 'Marketdate:') !!}
    <p>{!! $inventory->marketdate !!}</p>
</div>

<!-- Createdby Field -->
<div class="form-group">
    {!! Form::label('createdBy', 'Createdby:') !!}
    <p>{!! $inventory->createdBy !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $inventory->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $inventory->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $inventory->deleted_at !!}</p>
</div>

