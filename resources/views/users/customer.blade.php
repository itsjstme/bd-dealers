@extends('layouts.template')
@section('content') 
<link rel="stylesheet" href="{{URL::to('/')}}/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
<link rel="stylesheet" href="{{URL::to('/')}}/global/vendor/bootstrap-maxlength/bootstrap-maxlength.css">
<link rel="stylesheet" href="{{URL::to('/')}}/global/vendor/jt-timepicker/jquery-timepicker.css">
<link rel="stylesheet" href="{{URL::to('/')}}/assets/examples/css/forms/advanced.css">

<style>
p.redcolor{color:red; font-size:16px;}
.spancolor{color:red;}
.help-block{color:red;}
</style>

<div class="page-header">
  <h1 class="page-title font_lato">Create New Customer</h1>
  <div class="page-header-actions">
  <ol class="breadcrumb">
		<li><a href="{{URL::to('/dashboard')}}">{{ trans('app.home')}}</a></li>
		<li><a href="{{URL::to('userlist')}}">{{ trans('app.users')}}</a></li>
		<li class="active">{{ trans('app.create')}}</li>
	</ol>
  </div>
</div>
	
<div class="page-content" ng-app="app" ng-cloak>	
<div class="panel">
<div class="panel-body container-fluid">
<!------------------------start insert, update, delete message  ---------------->
<div class="row">
@if(session('msg_success'))
	<div class="alert dark alert-icon alert-success alert-dismissible alertDismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">×</span>
	  </button>
	  <i class="icon wb-check" aria-hidden="true"></i> 
	  {{session('msg_success')}}
	</div>
@endif
@if(session('msg_update'))
	<div class="alert dark alert-icon alert-info alert-dismissible alertDismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">×</span>
	  </button>
	  <i class="icon wb-check" aria-hidden="true"></i> 
	  {{session('msg_update')}}
	</div>
@endif
@if(session('msg_delete'))
	<div class="alert dark alert-icon alert-danger alert-dismissible alertDismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">×</span>
	  </button>
	  <i class="icon wb-check" aria-hidden="true"></i> 
	  {{session('msg_delete')}}
	</div>
@endif
</div>
<form  name="userForm" action="{{URL::to('/customer/registration')}}" ng-submit="submitForm(userForm.$valid)" novalidate  id="demo-form2" data-parsley-validate="" method="post" novalidate="">
		{{ csrf_field() }}
<div class="row row-lg">
	<div class="col-sm-8" style="border-right: 1px dotted #ddd;">
	  <!-- Example Basic Form -->	              
			<div class="row">
			  <div class="form-group col-sm-6">
				<label class="control-label" for="inputBasicFirstName">Dealership<span class="spancolor">*</span> </label>
				<input type="text" class="form-control" id="dealershipname" ng-model="dealershipname" name="dealershipname" ng-init="dealershipname='{{ old('dealershipname') }}'" placeholder="Dealership" required>
				 @if ($errors->has('dealershipname'))
				  <span class="help-block">
				   <strong>{{ $errors->first('dealershipname') }}</strong>
				  </span>
				 @endif				
			  </div>
			  <div class="form-group col-sm-3">
				<label class="control-label" for="inputBasicFirstName">{{ trans('app.first_name')}}<span class="spancolor">*</span> </label>
				<input type="text" class="form-control" id="first_name" ng-model="first_name" name="first_name" ng-init="first_name='{{ old('first_name') }}'" placeholder="{{ trans('app.first_name')}}" required>
				 @if ($errors->has('first_name'))
				  <span class="help-block">
				   <strong>{{ $errors->first('first_name') }}</strong>
				  </span>
				 @endif					
			  </div>
			  <div class="form-group col-sm-3">
				<label class="control-label" for="inputBasicLastName">{{ trans('app.last_name')}}<span class="spancolor">*</span> </label>
				<input type="text" class="form-control" id="last_name" name="last_name" ng-model="last_name" ng-init="last_name='{{ old('last_name') }}'" placeholder="{{ trans('app.last_name')}}" required>
				 @if ($errors->has('last_name'))
				  <span class="help-block">
				   <strong>{{ $errors->first('last_name') }}</strong>
				  </span>
				 @endif					
			  </div>
			  <div class="form-group col-sm-4">
				<label class="control-label" for="inputBasicFirstName">Credit Card Number<span class="spancolor">*</span> </label>
				<input class="form-control" id="number" ng-model="number" name="number" ng-init="number='{{ old('number') }}'" data-plugin="formatter" data-pattern="[[9999]]-[[9999]]-[[9999]]-[[9999]]" placeholder="Dealership" type="text" required>
				 @if ($errors->has('number'))
				  <span class="help-block">
				   <strong>{{ $errors->first('number') }}</strong>
				  </span>
				 @endif	
			  </div>
			  <div class="form-group col-sm-3">
				<label class="control-label" for="inputBasicFirstName">Expiration Month<span class="spancolor">*</span> </label>
					<select class="form-control" id="exp_month" ng-model="exp_month" name="exp_month" ng-init="exp_month='{{ old('exp_month') }}'" require>
						    <option selected value=''>--Select Month--</option>
						    <option value='1'>Janaury</option>
						    <option value='2'>February</option>
						    <option value='3'>March</option>
						    <option value='4'>April</option>
						    <option value='5'>May</option>
						    <option value='6'>June</option>
						    <option value='7'>July</option>
						    <option value='8'>August</option>
						    <option value='9'>September</option>
						    <option value='10'>October</option>
						    <option value='11'>November</option>
						    <option value='12'>December</option>
	                  </select>	
				 @if ($errors->has('exp_month'))
				  <span class="help-block">
				   <strong>{{ $errors->first('exp_month') }}</strong>
				  </span>
				 @endif		                  
			  </div>			  
			  <div class="form-group col-sm-3">
				<label class="control-label" for="inputBasicFirstName">Expiration Year<span class="spancolor">*</span> </label>
				<input type="text" class="form-control" id="exp_year" ng-model="exp_year" name="exp_year" ng-init="exp_year='{{ old('exp_year') }}'" placeholder="Enter Year" required>
				 @if ($errors->has('exp_year'))
				  <span class="help-block">
				   <strong>{{ $errors->first('exp_year') }}</strong>
				  </span>
				 @endif					
			  </div>
			  <div class="form-group col-sm-2">
				<label class="control-label" for="inputBasicLastName">CCV<span class="spancolor">*</span> </label>
				<input type="text" class="form-control" id="exp_ccv" name="exp_ccv" ng-model="exp_ccv" ng-init="exp_ccv='{{ old('exp_ccv') }}'" placeholder="CCV" required>
				 @if ($errors->has('exp_ccv'))
				  <span class="help-block">
				   <strong>{{ $errors->first('exp_ccv') }}</strong>
				  </span>
				 @endif					
			  </div>
				<div class="form-group col-sm-12">
				<label class="control-label" for="inputBasicFirstName">Billing Address<span class="spancolor">*</span></label>
				<textarea class="form-control" id="billingaddress" name="billingaddress" value="{{ old('billingaddress') }}" placeholder="Billing Address" autocomplete="off" required></textarea>
				 @if ($errors->has('billingaddress'))
				  <span class="help-block">
				   <strong>{{ $errors->first('billingaddress') }}</strong>
				  </span>
				 @endif					
			 	</div>			  
			</div>
			
			<div class="row">			  
			  <div class="form-group col-sm-4">
				<label class="control-label" for="inputBasicFirstName">{{ trans('app.phone')}}<span class="spancolor">*</span></label>
				<input type="text" class="form-control" id="phone" name="phone" placeholder="{{ trans('app.phone')}}" value="{{ old('phone') }}" autocomplete="off" data-plugin="formatter" data-pattern="([[999]]) [[999]]-[[9999]]" required>
				 @if ($errors->has('phone'))
				  <span class="help-block">
				   <strong>{{ $errors->first('phone') }}</strong>
				  </span>
				 @endif					
			  </div>			  
			  <div class="form-group col-sm-4">			  
			  <label class="control-label" for="inputBasicFirstName">{{ trans('app.date_of_birth')}}</label>
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="icon wb-calendar" aria-hidden="true"></i>
                    </span>
                    <input type="text" class="form-control" name="date_of_birth" value="{{ old('date_of_birth') }}" placeholder="{{ trans('app.date_of_birth')}}" data-plugin="datepicker">
                  </div>
                </div>			  
			  <div class="form-group col-sm-4">
				<label class="control-label">{{ trans('app.country')}} <span class="spancolor">*</span></label>
				<select ng-model="country"  class="form-control" name="country" required ng-init="country = '{{ old('country') }}'">
					<option value="">{{ trans('app.select_country')}} </option>	
					@foreach($country as $data)
						<option value="{{$data->nicename}}">{{$data->nicename}}</option>
					@endforeach
				</select>
				 @if ($errors->has('select_country'))
				  <span class="help-block">
				   <strong>{{ $errors->first('select_country') }}</strong>
				  </span>
				 @endif					
			  </div>			  
			</div>
			<div class="row">
				<div class="form-group col-sm-12">
				<label class="control-label" for="inputBasicFirstName">Notes</label>
				<textarea class="form-control" id="notes" name="notes" value="{{ old('notes') }}" placeholder="Notes" autocomplete="off"></textarea>
				<!--<input type="text" class="form-control" id="address" name="address" value="{{ old('address') }}" placeholder="{{ trans('app.address')}}" autocomplete="off">-->
			 	</div>
			  
			</div>
	  
	</div>
	<div class="col-sm-4">
	  <!-- Example Basic Form Without Label -->
		<div class="form-group">
		  <label class="control-label" for="inputBasicEmail">{{ trans('app.email_address')}} <span class="spancolor">*</span></label>
		  <input type="email" class="form-control" ng-model="email" ng-init="email='{{ old('email') }}'" required id="email" name="email" placeholder="{{ trans('app.email_address')}}" autocomplete="off">
		 @if ($errors->has('email'))
		  <span class="help-block">
		   <strong>{{ $errors->first('email') }}</strong>
		  </span>
		 @endif
		</div>
		<div class="form-group">
		<label class="control-label" for="inputBasicPassword"> {{ trans('app.password')}} <span class="spancolor">*</span></label>
			<input type="password" class="form-control"  ng-model="password" placeholder="{{ trans('app.password')}}" name="password" required="required" ng-init="password = '{{old('password')}}'" ng-minlength="6"/>
				 @if ($errors->has('password'))
				  <span class="help-block">
				   <strong>{{ $errors->first('password') }}</strong>
				  </span>
				 @endif	
		</div>
		<div class="form-group">
		<label class="control-label" for="inputBasicPassword">{{ trans('app.confirm_password')}} <span class="spancolor">*</span></label>
			<input type="password" class="form-control"  name="confirm_password" ng-model="confirm_password" ng-init="confirm_password = '{{old('confirm_password')}}'" placeholder="{{ trans('app.confirm_password')}}" match-password="password" required >
				 @if ($errors->has('confirm_password'))
				  <span class="help-block">
				   <strong>{{ $errors->first('confirm_password') }}</strong>
				  </span>
				 @endif	
		</div>
			
	</div>
	<div style="clear:both"></div>
	<div class="form-group col-sm-6">
		<button type="submit" ng-disabled="userForm.$invalid" class="btn btn-primary ladda-button" data-plugin="ladda" data-style="expand-left">
			<i class="fa fa-save"></i>  {{ trans('app.create_an_account')}}
		<span class="ladda-spinner"></span><div class="ladda-progress" style="width: 0px;"></div>
		</button>
	
	</div>
  </div>
</form> 
</div>
<!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
</div><br/>
		
<script>
var app = angular.module('app', []);

app.directive("matchPassword", function () {
    return {
        require: "ngModel",
        scope: {
            otherModelValue: "=matchPassword"
        },
        link: function(scope, element, attributes, ngModel) {

            ngModel.$validators.matchPassword = function(modelValue) {
                return modelValue == scope.otherModelValue;
            };

            scope.$watch("otherModelValue", function() {
                ngModel.$validate();
            });
        }
    };
});
</script>
@stop