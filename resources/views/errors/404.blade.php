<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="bootstrap admin template">
  <meta name="author" content="">
  <title>400</title>

  <!-- Stylesheets -->
  <link rel="stylesheet" href="http://crud-itsjstme.c9users.io/global/css/bootstrap.min.css">
  <link rel="stylesheet" href="http://crud-itsjstme.c9users.io/global/css/bootstrap-extend.min.css">
  <link rel="stylesheet" href="http://crud-itsjstme.c9users.io/assets/css/site.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="http://crud-itsjstme.c9users.io/global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="http://crud-itsjstme.c9users.io/global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="http://crud-itsjstme.c9users.io/global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="http://crud-itsjstme.c9users.io/global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="http://crud-itsjstme.c9users.io/global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="http://crud-itsjstme.c9users.io/global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="http://crud-itsjstme.c9users.io/assets/examples/css/pages/errors.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="http://crud-itsjstme.c9users.io/global/fonts/web-icons/web-icons.min.css">
  <link rel="stylesheet" href="http://crud-itsjstme.c9users.io/global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
  <!--[if lt IE 9]>
    <script src="../../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="http://crud-itsjstme.c9users.io/global/vendor/modernizr/modernizr.js"></script>
  <script src="http://crud-itsjstme.c9users.io/global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="page-error page-error-400 layout-full">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <!-- Page -->
  <div class="page animsition vertical-align text-center" data-animsition-in="fade-in"
  data-animsition-out="fade-out">
    <div class="page-content vertical-align-middle">
      <header>
        <h1 class="animation-slide-top">400</h1>
        <p>Page Not Found !</p>
      </header>
      <a class="btn btn-primary btn-round" href="http://crud-itsjstme.c9users.io/dashboard">GO TO HOME PAGE</a>
      <footer class="page-copyright">
        <p>&#169; <?php echo date('Y'); ?>. All RIGHT RESERVED.</p>
        <div class="social">
          <a href="javascript:void(0)">
            <i class="icon bd-twitter" aria-hidden="true"></i>
          </a>
          <a href="javascript:void(0)">
            <i class="icon bd-facebook" aria-hidden="true"></i>
          </a>
          <a href="javascript:void(0)">
            <i class="icon bd-dribbble" aria-hidden="true"></i>
          </a>
        </div>
      </footer>
    </div>
  </div>
  <!-- End Page -->
  <!-- Core  -->
  <script src="http://crud-itsjstme.c9users.io/global/vendor/jquery/jquery.js"></script>
  <script src="http://crud-itsjstme.c9users.io/global/vendor/bootstrap/bootstrap.js"></script>
  <script src="http://crud-itsjstme.c9users.io/global/vendor/animsition/animsition.js"></script>
  <script src="http://crud-itsjstme.c9users.io/global/vendor/asscroll/jquery-asScroll.js"></script>
  <script src="http://crud-itsjstme.c9users.io/global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="http://crud-itsjstme.c9users.io/global/vendor/asscrollable/jquery.asScrollable.all.js"></script>
  <script src="http://crud-itsjstme.c9users.io/global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <!-- Plugins -->
  <script src="http://crud-itsjstme.c9users.io/global/vendor/switchery/switchery.min.js"></script>
  <script src="http://crud-itsjstme.c9users.io/global/vendor/intro-js/intro.js"></script>
  <script src="http://crud-itsjstme.c9users.io/global/vendor/screenfull/screenfull.js"></script>
  <script src="http://crud-itsjstme.c9users.io/global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <!-- Scripts -->
  <script src="http://crud-itsjstme.c9users.io/global/js/core.js"></script>
  <script src="http://crud-itsjstme.c9users.io/assets/js/site.js"></script>
  <script src="http://crud-itsjstme.c9users.io/assets/js/sections/menu.js"></script>
  <script src="http://crud-itsjstme.c9users.io/assets/js/sections/menubar.js"></script>
  <script src="http://crud-itsjstme.c9users.io/assets/js/sections/gridmenu.js"></script>
  <script src="http://crud-itsjstme.c9users.io/assets/js/sections/sidebar.js"></script>
  <script src="http://crud-itsjstme.c9users.io/global/js/configs/config-colors.js"></script>
  <script src="http://crud-itsjstme.c9users.io/assets/js/configs/config-tour.js"></script>
  <script src="http://crud-itsjstme.c9users.io/global/js/components/asscrollable.js"></script>
  <script src="http://crud-itsjstme.c9users.io/global/js/components/animsition.js"></script>
  <script src="http://crud-itsjstme.c9users.io/global/js/components/slidepanel.js"></script>
  <script src="http://crud-itsjstme.c9users.io/global/js/components/switchery.js"></script>
  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();
    });
  })(document, window, jQuery);
  </script>
<link rel='stylesheet' type='text/css' property='stylesheet' href='//crud-itsjstme.c9users.io/_debugbar/assets/stylesheets?v=1491577526'><script type='text/javascript' src='//crud-itsjstme.c9users.io/_debugbar/assets/javascript?v=1491577526'></script><script type="text/javascript">jQuery.noConflict(true);</script>
</body>
</html>
