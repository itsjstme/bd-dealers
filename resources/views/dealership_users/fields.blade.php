<!-- Dealershipid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dealershipid', 'Dealershipid:') !!}
    {!! Form::number('dealershipid', null, ['class' => 'form-control']) !!}
</div>

<!-- Userid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('userid', 'Userid:') !!}
    {!! Form::number('userid', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('dealershipUsers.index') !!}" class="btn btn-default">Cancel</a>
</div>
