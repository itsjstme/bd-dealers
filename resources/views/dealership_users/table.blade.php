<table class="table table-responsive" id="dealershipUsers-table">
    <thead>
        <th>Dealershipid</th>
        <th>Userid</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($dealershipUsers as $dealershipUsers)
        <tr>
            <td>{!! $dealershipUsers->dealershipid !!}</td>
            <td>{!! $dealershipUsers->userid !!}</td>
            <td>
                {!! Form::open(['route' => ['dealershipUsers.destroy', $dealershipUsers->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('dealershipUsers.show', [$dealershipUsers->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('dealershipUsers.edit', [$dealershipUsers->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>