<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $dealershipUsers->id !!}</p>
</div>

<!-- Dealershipid Field -->
<div class="form-group">
    {!! Form::label('dealershipid', 'Dealershipid:') !!}
    <p>{!! $dealershipUsers->dealershipid !!}</p>
</div>

<!-- Userid Field -->
<div class="form-group">
    {!! Form::label('userid', 'Userid:') !!}
    <p>{!! $dealershipUsers->userid !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $dealershipUsers->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $dealershipUsers->updated_at !!}</p>
</div>

