@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Dealership
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($dealership, ['route' => ['dealerships.update', $dealership->id], 'method' => 'patch']) !!}

                        @include('dealerships.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection