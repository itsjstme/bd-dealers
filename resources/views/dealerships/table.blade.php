<table class="table table-responsive" id="dealerships-table">
    <thead>
        <th>Name</th>
        <th>Address</th>
        <th>City</th>
        <th>State</th>
        <th>Zip</th>
        <th>Phone</th>
        <th>Createdby</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($dealerships as $dealership)
        <tr>
            <td>{!! $dealership->name !!}</td>
            <td>{!! $dealership->address !!}</td>
            <td>{!! $dealership->city !!}</td>
            <td>{!! $dealership->state !!}</td>
            <td>{!! $dealership->zip !!}</td>
            <td>{!! $dealership->phone !!}</td>
            <td>{!! $dealership->createdBy !!}</td>
            <td>
                {!! Form::open(['route' => ['dealerships.destroy', $dealership->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('dealerships.show', [$dealership->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('dealerships.edit', [$dealership->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>